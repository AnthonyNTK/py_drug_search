import os
import sys
import json
import logging
import MetaData
import DrugSearch
import verb
import numpy as np

from keras.models import load_model
from flask import Flask, jsonify, request

app = Flask(__name__)
search_engine = DrugSearch.SearchEngine()


# Homepage
@app.route('/')
def index():
    return 'Hello world'


# endpoint for model prediction by syndrome
@app.route('/api/predict/syndrome', methods=['POST'])
def predict_by_syndrome():
    payload = request.data.decode('utf8').replace("'", '"')
    payload_dict = json.loads(payload)
    query_string = payload_dict['syndrome']

    return use_model(query_string.split())


# endpoint for disease search by group and symptoms
@app.route('/api/search/disease', methods=['POST'])
def search_disease():
    payload = request.data.decode('utf8').replace("'", '"')
    payload_dict = json.loads(payload)

    disease_group = payload_dict['disease']
    symptoms_description = ' '.join(payload_dict['syndrome'])

    instance = search_engine.initialize(
        mode='disease',
        index_file=DrugSearch.metadata.disease_index_file
    )
    doc_ids = instance.find_relevance(
        symptoms_description,
        disease_search=True
    )

    print(doc_ids)

    app.logger.info(doc_ids)

    return match_disease(disease_group, doc_ids)


# endpoint for extracting documents by id
@app.route('/api/search/id', methods=['POST'])
def find_relevant_by_id():
    payload = request.data.decode('utf8').replace("'", '"')
    payload_dict = json.loads(payload)

    return extract(payload_dict['doc_id'])


# endpoint for extracting documents by drugs name
@app.route('/api/search/drugs')
def search_drug():
    query = request.args.get('query')
    query = query.lower()

    instance = search_engine.initialize(
        mode='drugs',
        drug_file=DrugSearch.metadata.drug_index_file
    )
    doc_ids = instance.find_exact_drug(query)

    app.logger.info(doc_ids)

    return search(
        doc_ids,
        query,
        len(doc_ids),
        exact_search=True
    )


# endpoint for extracting documents by indication
@app.route('/api/search/indication')
def search_indication():
    query = request.args.get('query')
    query = query.lower()
    if not valid_query(query):
        return error()

    instance = search_engine.initialize(
        mode='indication',
        index_file=DrugSearch.metadata.indication_index_file,
        fda_file=DrugSearch.metadata.fda_indication_index_file
    )
    doc_ids = instance.find_relevance(query)

    app.logger.info(doc_ids)

    return search(doc_ids, query, len(doc_ids))


# endpoint for extracting documents by toxicity
@app.route('/api/search/toxicity')
def search_toxicity():
    query = request.args.get('query')
    query = query.lower()
    if not valid_query(query):
        return error()

    instance = search_engine.initialize(
        mode='toxicity',
        index_file=DrugSearch.metadata.toxicity_index_file,
        fda_file=DrugSearch.metadata.fda_toxicity_index_file
    )
    doc_ids = instance.find_relevance(query)

    app.logger.info(doc_ids)

    return search(doc_ids, query, len(doc_ids))


# endpoint for extracting documents by general description
@app.route('/api/search/description')
def search_description():
    query = request.args.get('query')
    query = query.lower()
    if not valid_query(query):
        return error()

    instance = search_engine.initialize(
        mode='description',
        index_file=DrugSearch.metadata.description_index_file,
        fda_file=DrugSearch.metadata.fda_description_index_file
    )
    doc_ids = instance.find_relevance(query)

    app.logger.info(doc_ids)

    return search(doc_ids, query, len(doc_ids))


# helper function to read drugs info by document ids
def extract(doc_ids):
    respond = {}
    result = []

    for doc_id in doc_ids:
        with open(MetaData.ROOT_DIR + '/result/' + str(doc_id) + '.txt', encoding='utf8') as target:
            data = json.load(target)
            result.append(data)

    respond['result'] = result
    return jsonify(respond)


def use_model(data):
    with open(MetaData.ROOT_DIR + '/disease/disease_label.txt', encoding='utf8') as mapping_file:
        disease_list = json.load(mapping_file)

    with open(MetaData.ROOT_DIR + '/disease/word_index.txt', encoding='utf8') as word_file:
        word_list = json.load(word_file)

    query = []
    for word in data:
        try:
            query.append(word_list[word])
        except KeyError:
            continue

    if len(query) == 0:
        return no_prediction()

    x_test = vectorize_sequences(query)
    model = load_model(MetaData.ROOT_DIR + '/disease/my_model.h5')

    id_list = []
    predictions = model.predict(x_test)
    for prediction in predictions:
        id_list.append(np.argmax(prediction))

    if len(id_list) > 0:
        disease = []
        for disease_id in id_list:
            for i, disease_name in enumerate(disease_list.keys()):
                if i == disease_id:
                    disease.append(disease_name)

        respond = {
            'disease': disease,
            'syndrome': data
        }
        return jsonify(respond)
    else:
        return no_prediction()


def vectorize_sequences(sequences, dimension=5900):
    results = np.zeros((len(sequences), dimension))
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1.

    return results


# helper function to find matched disease within provided disease group
def match_disease(disease_group, extracted_id):
    valid_doc_id = []
    result_doc_id = []
    result_doc = []

    with open(MetaData.ROOT_DIR + '/disease/disease_label.txt', encoding='utf8') as label_file:
        label_map = json.load(label_file)

    for disease in disease_group:
        valid_doc_id += label_map[disease]

    for doc_id in extracted_id:
        if doc_id in valid_doc_id:
            result_doc_id.append(doc_id)
            with open(MetaData.ROOT_DIR + '/disease/mapping/' + str(doc_id) + '.txt') as target:
                data = json.load(target)
                result_doc.append(data)

    respond = {}

    if len(result_doc) == 0:
        return no_prediction()

    if len(result_doc) <= 10:
        respond['disease'] = result_doc
        respond['history'] = []
    else:
        respond['disease'] = result_doc[:10]
        respond['history'] = result_doc_id[10:]

    respond['length'] = len(result_doc)
    return jsonify(respond)


# helper function to extract drug info if id(s) is/are provided,
# return word suggestions or respond to exception
def search(doc_ids, query, result_length, exact_search=False):
    if result_length > 0:
        result = []                             # array to store all drugs information read from files
        history = []                            # array to store the id of top10 records for firebase history recording
        batch = []                              # array to store all ids of search result for pagination
        respond = {}                            # dict for http respond

        lower_bound = 10                        # pagination limit
        if result_length < lower_bound:
            lower_bound = result_length

        first_batch = doc_ids[0:lower_bound]
        next_batch = doc_ids[lower_bound:]
        for doc_id in next_batch:
            batch.append(doc_id[0])

        for doc_id in first_batch:
            if exact_search:
                with open(MetaData.ROOT_DIR + '/result/' + str(doc_id['doc_id']) + '.txt') as target:
                    data = json.load(target)
                    result.append(data)
                    history.append(doc_id['doc_id'])
            else:
                with open(MetaData.ROOT_DIR + '/result/' + str(doc_id[0]) + '.txt') as target:
                    data = json.load(target)
                    result.append(data)
                    history.append(doc_id[0])

        respond['query'] = query
        respond['length'] = result_length
        respond['result'] = result
        respond['batch'] = batch
        respond['doc-id'] = history
        return jsonify(respond)
    else:
        if exact_search:
            return error()

        metadata = MetaData.MetaData()
        checker = verb.Tense()                  # helper class to check verb tense
        checker.initialize()

        suggested_words = []                    # array to store all word suggestions

        query_keywords = query.split(' ')
        filtered_words = [word for word in query_keywords if word not in metadata.stopwords]
        for filtered_word in filtered_words:

            # modify word into original form (e.g. sneezing --> sneeze, sneezed --> sneeze, etc.)
            potential_verb = checker.verb_infinitive(filtered_word)

            # try if words can be transform back to original form by identifying the current tense
            if potential_verb != filtered_word \
                    and (not checker.verb_is_tense(filtered_word, 'pl')
                         or not checker.verb_is_tense(filtered_word, '1sgpres')
                         or not checker.verb_is_tense(filtered_word, '2sgpres')
                         or not checker.verb_is_tense(filtered_word, '3sgpres')
                         or not checker.verb_is_tense(filtered_word, 'inf')):
                if potential_verb != "":
                    suggested_words.append(potential_verb)

                    # check if words exist in indexing dicy
                    for word in search_engine.index.keys():
                        if potential_verb in word:
                            suggested_words.append(word)
            else:
                for word in search_engine.index.keys():
                    if filtered_word in word:
                        suggested_words.append(word)

        if len(suggested_words) > 0:
            unique_suggestions = list(set(suggested_words))
            suggestion_respond = {"error": 'No related result found', "suggestions": unique_suggestions}
            return jsonify(suggestion_respond), 300

        return error()


# check if query is alphanumeric and containing only '-'
def valid_query(query):
    if all(char.isalpha() or char.isspace() or char == '-' for char in query):
        return True

    return False


def error():
    error_respond = {'error': 'No related result found'}
    return jsonify(error_respond), 400


def no_prediction():
    respond = {'error': 'No prediction can be made'}
    return jsonify(respond), 400


if __name__ == '__main__':
    if 'DYNO' in os.environ:
        app.logger.addHandler(logging.StreamHandler(sys.stdout))
        app.logger.setLevel(logging.ERROR)

    app.run()
