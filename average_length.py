import os
import re
import time
import json
import threading
import collections
import MetaData

metadata = MetaData.MetaData()


def read_file(doc_list, target, destination):
    doc_length = 0

    length_stat = collections.defaultdict(int)

    for doc in doc_list:
        with open(target + str(doc) + '.txt', encoding='utf8') as input_file:
            content = input_file.read()
            tokens = content.split()

            length_stat[doc] = len(tokens)
            doc_length += len(tokens)

    with open(destination, 'w', encoding='utf8') as output_file:
        output_file.write(json.dumps(length_stat))

    print(target[:-1] + ' total length: ' + str(doc_length))
    print(target[:-1] + ' average length: ' + str(doc_length / len(doc_list)))


def read_disease_file(destination):
    doc_length = 0

    length_stat = collections.defaultdict(int)

    with open('disease/disease_label.txt', encoding='utf8') as label_file:
        labels = json.load(label_file)

    doc_id_list = []

    for key, value in labels.items():
        for doc in value:
            doc_id_list.append(doc)

    doc_id_list.sort()
    for doc in doc_id_list:
        file_path = os.path.join(metadata.disease_target, str(doc) + '.txt')
        with open(file_path, encoding='utf8') as input_file:
            description = input_file.read()
            tokens = description.split()

            length_stat[doc] = len(tokens)
            doc_length += len(tokens)

    with open(destination, 'w', encoding='utf8') as output_file:
        output_file.write(json.dumps(length_stat))

    print('disease total length: ' + str(doc_length))
    print('disease average length: ' + str(doc_length / len(doc_id_list)))


def main():
    t0 = time.time()

    indication_doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string
                              in os.listdir(MetaData.ROOT_DIR + '/external/fda_indication')]
    indication_doc_id_list.sort()

    toxicity_doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string
                            in os.listdir(MetaData.ROOT_DIR + '/external/fda_toxicity')]
    toxicity_doc_id_list.sort()

    description_doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string
                               in os.listdir(MetaData.ROOT_DIR + '/external/fda_description')]
    description_doc_id_list.sort()

    threads = [
        threading.Thread(
            target=read_file,
            args=[indication_doc_id_list, 'external/fda_indication/', metadata.fda_indication_average_file]
        ),
        threading.Thread(
            target=read_file,
            args=[toxicity_doc_id_list, 'external/fda_toxicity/', metadata.fda_toxicity_average_file]
        ),
        threading.Thread(
            target=read_file,
            args=[description_doc_id_list, 'external/fda_description/', metadata.fda_description_average_file]
        )
        # threading.Thread(
        #     target=read_disease_file,
        #     args=[metadata.disease_average_file]
        # )
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    t1 = time.time()
    print('Processing time: ' + str(t1 - t0))


if __name__ == '__main__':
    main()
