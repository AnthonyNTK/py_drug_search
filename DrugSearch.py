import json
import MetaData
import operator
import threading
import collections

metadata = MetaData.MetaData()      # external class storing all project-related metadata


class SearchEngine:
    def __init__(self):
        self.drug = {}                              # dict to store all drug name
        self.index = {}                             # dict to store all index of all terms
        self.fda_index = {}                         # dict to store all index of all terms from fda
        self.wiki_score = {}                        # dict to store all score of drugs from wiki
        self.cosine_similarity = {}                 # dict to store all cosine similarity of each document
        self.valid_count = 0                        # number of valid keywords provided
        self.previous_search_endpoint = ''          # flag to determine search mode

        self.initialize_cosine_similarity()

    def initialize_cosine_similarity(self):
        for i in range(MetaData.FILE_NUM):
            self.cosine_similarity[i] = 0

    def find_exact_drug(self, query):
        drugs = []
        for drug_name in self.drug.keys():
            if query in drug_name.lower():
                doc_tuple = {'doc_id': self.drug[drug_name], 'name': drug_name}
                drugs.append(doc_tuple)

        drugs.sort(key=lambda x: len(x['name']), reverse=False)
        return drugs

    def find_relevance(self, query, disease_search=False):
        doc_list = collections.defaultdict(int)

        # filter all non-meaningful words
        query_keywords = query.split(' ')
        filtered_words = [word for word in query_keywords if word not in metadata.stopwords]
        self.valid_count = len(filtered_words)
        for filtered_word in filtered_words:
            try:
                relevant_docs = self.index[filtered_word]  # find all related document
            except KeyError:
                return []

            try:
                fda_docs = self.fda_index[filtered_word]   # fina all fda document
            except KeyError:
                fda_docs = {}

            for relevant_doc in relevant_docs:  # all documents with occurrence of that word
                document = relevant_doc[0]
                score = relevant_doc[1]
                self.cosine_similarity[document] += score  # sum up their vector length

                doc_list[document] += 1

            for fda_doc in fda_docs:
                document = fda_doc[0]
                score = fda_doc[1]
                self.cosine_similarity[document] = self.cosine_similarity[document] * 0.8 + score * 0.2

        for doc_key in self.cosine_similarity:
            try:
                wiki_score = self.wiki_score[doc_key]   # fina all fda document
            except KeyError:
                wiki_score = 0

            self.cosine_similarity[doc_key] = self.cosine_similarity[doc_key] * 0.8 + wiki_score * 0.2

        sorted_similarity = sorted(self.cosine_similarity.items(), key=operator.itemgetter(1), reverse=True)

        valid_doc = []
        for doc in sorted_similarity:
            doc_id = doc[0]
            self.cosine_similarity[doc_id] = 0

            if disease_search:
                valid_doc.append(doc_id)
            elif doc_list[doc_id] == self.valid_count:
                valid_doc.append(doc)

        return valid_doc

    # turn inverted index mapping array into dictionary
    def read_term_index(self, index_file):
        self.index.clear()

        with open(index_file, encoding='utf8') as inverted_index:
            for line in inverted_index:
                data = json.loads(line)
                self.index[data[0]] = data[2]

    def read_fda_index(self, fda_index_file):
        self.fda_index.clear()

        with open(fda_index_file, encoding='utf8') as inverted_index:
            for line in inverted_index:
                data = json.loads(line)
                self.fda_index[data[0]] = data[2]

    def read_wiki_score(self, wiki_file):
        if not self.wiki_score:
            with open(wiki_file, encoding='utf8') as wiki_scores:
                self.wiki_score = json.loads(wiki_scores.read())

    def read_drug_name(self, drug_file):
        if not self.drug:
            with open(drug_file, encoding='utf8') as drug_names:
                for line in drug_names:
                    token = line.split(':')
                    drug_name = token[1].replace('\n', '')
                    self.drug[drug_name] = token[0]

    def initialize(self, mode, fda_file=None, index_file=None, drug_file=None):
        if mode != self.previous_search_endpoint:
            self.previous_search_endpoint = mode
            self.initialize_cosine_similarity()

            threads = []

            if fda_file is not None:
                fda_thread = threading.Thread(target=self.read_fda_index, args=[fda_file])
                threads.append(fda_thread)
                fda_thread.start()

            if index_file is not None:
                index_thread = threading.Thread(target=self.read_term_index, args=[index_file])
                threads.append(index_thread)
                index_thread.start()

            if drug_file is not None:
                drug_thread = threading.Thread(target=self.read_drug_name, args=[drug_file])
                threads.append(drug_thread)
                drug_thread.start()

            wiki_thread = threading.Thread(target=self.read_wiki_score, args=[metadata.wiki_score_file])
            threads.append(wiki_thread)
            wiki_thread.start()

            for thread in threads:
                thread.join()

        return self
