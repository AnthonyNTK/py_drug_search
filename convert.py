import json
import time
import nltk
import collections


def extract_target(description):
    valid_line = ''

    lines = nltk.sent_tokenize(description)
    for line in lines:
        if 'treatment of ' in line:
            valid_line = line
            break

    index = 0
    potential_target = collections.defaultdict(list)

    words = nltk.word_tokenize(valid_line)
    pos_tags = nltk.pos_tag(words)
    for word, pos in pos_tags:
        if pos == 'JJ':
            potential_target[index].append(word)
        elif pos == 'NN' and word != 'treatment':
            potential_target[index].append(word)
            index += 1

    content = ''
    for index in potential_target.keys():
        content += ' '.join(potential_target[index]) + ','

    return content[:-1]


def load():
    name_file = open('statistic/drug_name_index.txt', 'w', encoding='utf8')

    with open('valid.json', encoding='utf8') as json_file:
        index = 0

        data_set = json.load(json_file)
        for data in data_set:
            file_name = str(index)
            with open('result/' + file_name + '.txt', 'w', encoding='utf8') as output_doc:
                output_doc.write(json.dumps(data))

            if data['name'] is not None:
                name_file.write(file_name + ':' + data['name'] + '\n')

            if data['toxicity'] is not None:
                write_other_data(data['toxicity'], 'toxicity/' + file_name + '.txt')

            if data['indication'] is not None:
                write_other_data(data['indication'], 'indication/' + file_name + '.txt')

                syndromes = extract_target(data['indication'])
                if len(syndromes) > 1:
                    write_other_data(syndromes, 'important/' + file_name + '.txt')

            if data['description'] is not None:
                write_other_data(data['description'], 'description/' + file_name + '.txt')

            index += 1
            print(index)

    name_file.close()


def write_other_data(data, file_name):
    with open(file_name, 'w', encoding='utf8') as output_doc:
        output_doc.write(data.replace('\n', ''))


def main():
    start = time.time()

    load()

    end = time.time()
    print('Process time: ' + str(end - start))


if __name__ == '__main__':
    main()
