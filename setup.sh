#!/bin/bash
# Program:
#	User inputs arguments and program will populate the project
#   with different project structure and data
# History:
#   11/16/2018	Anthony	 v0.1 release
#   12/12/2018  Anthony  v0.2 release
#   01/01/2019  Anthony  v0.3 release

case ${1} in
    "dev")
    dev_setup
    ;;
    "debug")
    debug_setup
    ;;
    "prod")
    prod_setup
    ;;
esac

dev_setup()
{
    if [[ ! -d "./description" ]]; then
        mkdir description
        echo Folder 'description' created!
    else
        echo Folder 'description' already exist!
    fi

    if [[ ! -d "./indication" ]]; then
        mkdir indication
        echo Folder 'indication' created!
    else
        echo Folder 'indication' already exist!
    fi

    if [[ ! -d "./toxicity" ]]; then
        mkdir toxicity
        echo Folder 'toxicity' created!
    else
        echo Folder 'toxicity' already exist!
    fi

    if [[ ! -d "./statistic" ]]; then
        mkdir statistic
        echo Folder 'statistic' created!
    else
        echo Folder 'statistic' already exist!
    fi

    if [[ ! -d "./external" ]]; then
        mkdir external
        echo Folder 'external' created!
    else
        echo Folder 'external' already exist!
    fi

    if [[ ! -d "./important" ]]; then
        mkdir important
        echo Folder 'important' created!
    else
        echo Folder 'important' already exist!
    fi

    if [[ ! -d "./symptom" ]]; then
        mkdir symptom
        echo Folder 'symptom' created!
    else
        echo Folder 'symptom' already exist!
    fi

    if [[ ! -d "./result" ]]; then
        mkdir result
        echo Folder 'result' created!
    else
        echo Folder 'result' already exist!
    fi
}

debug_setup()
{
    if [[ ! -d "./description" ]] && [[ ! -d "./indication" ]] && [[ ! -d "./toxicity" ]]; then
        sudo apt-get install unzip
        unzip backup.zip
    else
        echo Folders already exists!
    fi
}

prod_setup()
{
    if [[ ! -d "./statistic" ]] && [[ ! -d "./result" ]]; then
        unzip convert.zip || sudo apt-get install unzip && unzip convert.zip || echo Failed to install unzip
    else
        echo Folders already exists!
    fi

    if [[ ! -d "./disease/mapping" ]]; then
        unzip disease/mapping.zip -d $PWD/disease || sudo apt-get install unzip && unzip disease/mapping.zip -d $PWD/disease || echo Failed to install unzip
        unzip convert.zip
    else
        echo Folders already exists!
    fi
}