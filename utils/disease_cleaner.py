import os
import re
import json

doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir('../disease/mapping')]
doc_id_list.sort()

with open('../disease/disease_label.txt', encoding='utf8') as label_file:
    labels = json.load(label_file)

valid_id_list = []

for key, value in labels.items():
    for doc in value:
        valid_id_list.append(doc)

valid_id_list.sort()

for full_doc in doc_id_list:
    if full_doc not in valid_id_list:
        try:
            os.remove('../disease/mapping/' + str(full_doc) + '.txt')
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))