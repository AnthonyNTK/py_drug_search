import os
import json
import MetaData

METADATA = MetaData.MetaData()
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


file_path = os.path.join(ROOT_DIR, METADATA.toxicity_index_file)
with open(file_path, encoding='utf8') as input_file:
    lines = input_file.readlines()
    for i in range(0, len(lines) - 1):
        data = json.loads(lines[i])
        data2 = json.loads(lines[i + 1])
        count = 0
        for char1, char2 in zip(data[0], data2[0]):
            if char1 == char2:
                count += 1

        if len(str(data[0])) > len(str(data[1])):
            if count > len(str(data[0])) / 2:
                print(data[0])
                print(data2[0])
                print(data[2])
                print(data2[2])
        elif len(str(data[0])) < len(str(data[1])):
            if count > len(str(data[1])) / 2:
                print(data[0])
                print(data2[0])
                print(data[2])
                print(data2[2])
