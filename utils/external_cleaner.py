import re
import os
import time
import exception


def clean_file():
    doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir('../external')]
    doc_id_list.sort()

    for docID in doc_id_list:
        print(docID)
        with open('../external/' + str(docID) + '.txt', encoding='utf8') as input_file:
            content = input_file.read()

        content = re.sub(exception.wiki_content_exception, '', content)
        content = re.sub(exception.unicode_exception, '', content)

        with open('../external/' + str(docID) + '.txt', 'w', encoding='utf8') as output_file:
            output_file.write(content.strip('\n'))


def main():
    t0 = time.time()

    clean_file()

    t1 = time.time()
    print('Processing time: ' + str(t1 - t0))


if __name__ == '__main__':
    main()