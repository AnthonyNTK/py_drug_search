import os
import re
import exception

doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir('../external')]
doc_id_list.sort()
for doc_id in doc_id_list:
    with open('../external/' + str(doc_id) + '.txt', encoding='utf8') as input_file:
        content = input_file.read()

        valid = True

        for invalid_content in exception.wiki_exception:
            if invalid_content in content:
                valid = False

        if not valid:
            print(doc_id)