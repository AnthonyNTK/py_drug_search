import os
import re
import json
import time
import math
import requests
import threading
import exception
from bs4 import BeautifulSoup

ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'result')
THREAD_LIMIT = 400


class Crawler:
    def __init__(self):
        self.url_list = []
        self.fda_data = []

    @staticmethod
    def download_wiki(keyword):
        try:
            url = 'https://en.wikipedia.org/wiki/' + keyword
            data = requests.get(
                url,
                headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0'}
            ).content
            return data
        except Exception as e:
            print('Exception at method donwload_link: ' + str(e))
            return b''

    @staticmethod
    def download_fda(keyword):
        try:
            url = 'https://api.fda.gov/drug/label.json?search=indications_and_usage:%22' + keyword + \
                  '%22+overdosage:%22' + keyword + \
                  '%22+dosage_and_administration:%22' + keyword + \
                  '%22+adverse_reactions:%22' + keyword + '%22'
            data = json.loads(requests.get(
                url,
                headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0'}
            ).text)

            if 'meta' in data:
                if data['meta']['results']['total'] <= 20:
                    print(url)
                    return data['results']
            elif 'error' in data:
                print(data['error']['code'] + ': ' + keyword)
            else:
                print('Unknown key: ' + keyword)
        except Exception as e:
            print('Exception at method donwload_link: (' + keyword + ') ' + str(e))

        return []

    @staticmethod
    def wiki_content_extractor(html, id):
        soup = BeautifulSoup(html, 'html.parser')
        try:
            content_soup = soup.find('div', attrs={'class': 'mw-body'})
            tags = content_soup.findAll('p')

            content = ''
            for tag in tags:
                content += tag.get_text()

            if 'medicine' in content or 'drug' in content:
                return True, content

            return False, ''
        except Exception as e:
            print('Exception at method wiki_content_extractor: (' + str(id) + ') ' + str(e))
            return False, ''

    def read_files(self, start, end):
        for docID in range(start, end):
            file_path = os.path.join(ROOT_DIR, str(docID) + '.txt')
            input_file = open(file_path, encoding='utf8')
            data = json.load(input_file)
            input_file.close()

            keyword = re.sub(' ', '_', data['name'])
            html = Crawler.download_wiki(keyword)
            if len(html) == 0:
                continue

            valid, content = Crawler.wiki_content_extractor(html, docID)

            for invalid_content in exception.wiki_exception:
                if invalid_content in content:
                    valid = False
                    break

            if valid and len(content) > 5:
                self.url_list.append(str(docID) + '::https://en.wikipedia.org/wiki/' + keyword)


def main():
    doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir(ROOT_DIR)]
    doc_id_list.sort()
    doc_length = len(doc_id_list)

    threads = []
    num_of_thread = int(math.floor(doc_length / THREAD_LIMIT))

    crawler = Crawler()

    t0 = time.time()

    # # split crawling job into thread for better performance
    # for i in range(0, num_of_thread):
    #     thread = threading.Thread(target=crawler.read_files, args=[i * THREAD_LIMIT, (i + 1) * THREAD_LIMIT])
    #     threads.append(thread)
    #     thread.start()
    #
    # # handle mod != 0 case
    # if doc_length > THREAD_LIMIT * num_of_thread:
    #     extra_thread = threading.Thread(target=crawler.read_files, args=[THREAD_LIMIT * num_of_thread, doc_length])
    #     threads.append(extra_thread)
    #     extra_thread.start()
    #
    # for thread in threads:
    #     thread.join()

    for doc_id in range(6608, 11293):
        file_path = os.path.join(ROOT_DIR, str(doc_id) + '.txt')
        input_file = open(file_path, encoding='utf8')
        data = json.load(input_file)
        input_file.close()

        fda_data = Crawler.download_fda(data['name'])
        if len(fda_data) != 0:
            if 'dosage_and_administration' in fda_data[0]:
                with open('./external/fda_description/' + str(doc_id) + '.txt', 'w', encoding='utf-8') as description_file:
                    description_file.write(fda_data[0]['dosage_and_administration'][0])

            if 'adverse_reactions' in fda_data[0]:
                with open('./external/fda_toxicity/' + str(doc_id) + '.txt', 'w', encoding='utf-8') as toxicity_file:
                    toxicity_file.write(fda_data[0]['adverse_reactions'][0])

            if 'indications_and_usage' in fda_data[0]:
                with open('./external/fda_indication/' + str(doc_id) + '.txt', 'w', encoding='utf-8') as indication_file:
                    indication_file.write(fda_data[0]['indications_and_usage'][0])

    # statistic
    t1 = time.time()
    print('Num of Thread: ' + str(len(threads)))
    print('Num of Doc: ' + str(doc_length))
    print('Num of valid Doc: ' + str(len(crawler.url_list)))
    print('Processing time: ' + str(t1 - t0))

    with open('external/wiki_link.txt', 'w', encoding='utf-8') as output:
        for url in crawler.url_list:
            output.write(url + '\n')


if __name__ == '__main__':
    main()
