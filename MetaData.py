import os
import re

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
FILE_NUM = len([int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir(ROOT_DIR + '\\result')])


class MetaData:
    def __init__(self):
        self.drug_index_file = ROOT_DIR + '\\statistic\\drug_name_index.txt'

        self.wiki_score_file = ROOT_DIR + '\\external\\wiki_score.txt'

        self.toxicity_total_length = 103007
        self.toxicity_average_length = 51.22178020885132
        self.toxicity_target = ROOT_DIR + '\\toxicity'
        self.toxicity_set_file = ROOT_DIR + '\\statistic\\toxicity_set.txt'
        self.toxicity_index_file = ROOT_DIR + '\\statistic\\toxicity_index.txt'
        self.toxicity_length_file = ROOT_DIR + '\\statistic\\toxicity_length.txt'
        self.toxicity_average_file = ROOT_DIR + '\\statistic\\toxicity_average.txt'

        self.indication_total_length = 95797
        self.indication_average_length = 25.532249466950958
        self.indication_target = ROOT_DIR + '\\indication'
        self.indication_set_file = ROOT_DIR + '\\statistic\\indication_set.txt'
        self.indication_index_file = ROOT_DIR + '\\statistic\\indication_index.txt'
        self.indication_length_file = ROOT_DIR + '\\statistic\\indication_length.txt'
        self.indication_average_file = ROOT_DIR + '\\statistic\\indication_average.txt'

        self.description_total_length = 314194
        self.description_average_length = 49.1850344395742
        self.description_target = ROOT_DIR + '\\description'
        self.description_set_file = ROOT_DIR + '\\statistic\\description_set.txt'
        self.description_index_file = ROOT_DIR + '\\statistic\\description_index.txt'
        self.description_length_file = ROOT_DIR + '\\statistic\\description_length.txt'
        self.description_average_file = ROOT_DIR + '\\statistic\\description_average.txt'

        self.disease_total_length = 67998
        self.disease_average_length = 25.16580310880829
        self.disease_target = ROOT_DIR + '\\symptom'
        self.disease_set_file = ROOT_DIR + '\\statistic\\disease_set.txt'
        self.disease_index_file = ROOT_DIR + '\\statistic\\disease_index.txt'
        self.disease_length_file = ROOT_DIR + '\\statistic\\disease_length.txt'
        self.disease_average_file = ROOT_DIR + '\\statistic\\disease_average.txt'

        self.fda_toxicity_total_length = 845648
        self.fda_toxicity_average_length = 860.2726347914547
        self.fda_toxicity_target = ROOT_DIR + '\\external\\fda_toxicity'
        self.fda_toxicity_set_file = ROOT_DIR + '\\statistic\\fda_toxicity_set.txt'
        self.fda_toxicity_index_file = ROOT_DIR + '\\statistic\\fda_toxicity_index.txt'
        self.fda_toxicity_length_file = ROOT_DIR + '\\statistic\\fda_toxicity_length.txt'
        self.fda_toxicity_average_file = ROOT_DIR + '\\statistic\\fda_toxicity_average.txt'

        self.fda_indication_total_length = 173522
        self.fda_indication_average_length = 153.28798586572438
        self.fda_indication_target = ROOT_DIR + '\\external\\fda_indication'
        self.fda_indication_set_file = ROOT_DIR + '\\statistic\\fda_indication_set.txt'
        self.fda_indication_index_file = ROOT_DIR + '\\statistic\\fda_indication_index.txt'
        self.fda_indication_length_file = ROOT_DIR + '\\statistic\\fda_indication_length.txt'
        self.fda_indication_average_file = ROOT_DIR + '\\statistic\\fda_indication_average.txt'

        self.fda_description_total_length = 631605
        self.fda_description_average_length = 556.4801762114538
        self.fda_description_target = ROOT_DIR + '\\external\\fda_description'
        self.fda_description_set_file = ROOT_DIR + '\\statistic\\fda_description_set.txt'
        self.fda_description_index_file = ROOT_DIR + '\\statistic\\fda_description_index.txt'
        self.fda_description_length_file = ROOT_DIR + '\\statistic\\fda_description_length.txt'
        self.fda_description_average_file = ROOT_DIR + '\\statistic\\fda_description_average.txt'

        self.stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'you\'re', 'you\'ve',
                          'you\'ll', 'you\'d', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself',
                          'she', 'she\'s', 'her', 'hers', 'herself', 'it', 'it\'s', 'its', 'itself', 'they', 'them',
                          'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'that\'ll',
                          'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has',
                          'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or',
                          'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against',
                          'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from',
                          'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once',
                          'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more',
                          'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than',
                          'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'don\'t', 'should', 'should\'ve',
                          'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', 'aren\'t', 'couldn', 'couldn\'t',
                          'didn', 'didn\'t', 'doesn', 'doesn\'t', 'hadn', 'hadn\'t', 'hasn', 'hasn\'t', 'haven',
                          'haven\'t', 'isn', 'isn\'t', 'ma', 'mightn', 'mightn\'t', 'mustn', 'mustn\'t', 'needn',
                          'needn\'t', 'shan', 'shan\'t', 'shouldn', 'shouldn\'t', 'wasn', 'wasn\'t', 'weren',
                          'weren\'t', 'won', 'won\'t', 'wouldn', 'wouldn\'t', 'cannot', 'dont', 'whoever', 'therefore',
                          'consequently', 'furthermore', 'whereas', 'nonetheless', 'although', 'nevertheless',
                          'whatever', 'however', 'besides', 'henceforward', 'yet', 'until', 'alternatively',
                          'meanwhile', 'notwithstanding', 'whenever', 'moreover', 'despite', 'similarly', 'firstly',
                          'secondly', 'lastly', 'eventually', 'gradually', 'finally', 'thus', 'hence', 'accordingly',
                          'otherwise', 'indeed', 'though', 'unless', 'never', 'also', 'many', 'much']

        self.process_metadata = []

    def set_toxicity_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.toxicity_target)
        self.process_metadata.append(self.toxicity_set_file)
        self.process_metadata.append(self.toxicity_index_file)
        self.process_metadata.append(self.toxicity_length_file)
        self.process_metadata.append(self.toxicity_average_file)
        self.process_metadata.append(self.toxicity_average_length)

    def set_description_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.description_target)
        self.process_metadata.append(self.description_set_file)
        self.process_metadata.append(self.description_index_file)
        self.process_metadata.append(self.description_length_file)
        self.process_metadata.append(self.description_average_file)
        self.process_metadata.append(self.description_average_length)

    def set_indication_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.indication_target)
        self.process_metadata.append(self.indication_set_file)
        self.process_metadata.append(self.indication_index_file)
        self.process_metadata.append(self.indication_length_file)
        self.process_metadata.append(self.indication_average_file)
        self.process_metadata.append(self.indication_average_length)

    def set_fda_toxicity_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.fda_toxicity_target)
        self.process_metadata.append(self.fda_toxicity_set_file)
        self.process_metadata.append(self.fda_toxicity_index_file)
        self.process_metadata.append(self.fda_toxicity_length_file)
        self.process_metadata.append(self.fda_toxicity_average_file)
        self.process_metadata.append(self.fda_toxicity_average_length)

    def set_fda_description_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.fda_description_target)
        self.process_metadata.append(self.fda_description_set_file)
        self.process_metadata.append(self.fda_description_index_file)
        self.process_metadata.append(self.fda_description_length_file)
        self.process_metadata.append(self.fda_description_average_file)
        self.process_metadata.append(self.fda_description_average_length)

    def set_fda_indication_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.fda_indication_target)
        self.process_metadata.append(self.fda_indication_set_file)
        self.process_metadata.append(self.fda_indication_index_file)
        self.process_metadata.append(self.fda_indication_length_file)
        self.process_metadata.append(self.fda_indication_average_file)
        self.process_metadata.append(self.fda_indication_average_length)

    def set_disease_metadata(self):
        self.process_metadata.clear()
        self.process_metadata.append(self.disease_target)
        self.process_metadata.append(self.disease_set_file)
        self.process_metadata.append(self.disease_index_file)
        self.process_metadata.append(self.disease_length_file)
        self.process_metadata.append(self.disease_average_file)
        self.process_metadata.append(self.disease_average_length)
