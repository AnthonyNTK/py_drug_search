import os
import json
import time
import MetaData

metadata = MetaData.MetaData()


def read_file():
    with open('disease_label.txt', encoding='utf8') as label_file:
        labels = json.load(label_file)

    doc_id_list = []

    for key, value in labels.items():
        for doc in value:
            doc_id_list.append(doc)

    doc_id_list.sort()
    print(doc_id_list)

    output_file = open(metadata.process_metadata[1], 'w', encoding='utf8')

    for docID in doc_id_list:
        file_path = os.path.join(metadata.process_metadata[0], str(docID) + '.txt')
        with open(file_path, encoding='utf8') as input_file:
            description = input_file.read()
            record = [docID, description]
            output_file.write(json.dumps(record) + '\n')

        print(str(docID))

    output_file.close()


def main():
    t0 = time.time()

    metadata.set_disease_metadata()
    read_file()

    t1 = time.time()
    print('Processing time: ' + str(t1 - t0))


if __name__ == '__main__':
    main()
