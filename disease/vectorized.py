import os
import re
import json
import nltk
import time
import collections
import MetaData
import numpy as np
import matplotlib.pyplot as plt

from keras import models
from keras import layers
from keras.models import load_model
from keras.callbacks import ModelCheckpoint

metadata = MetaData.MetaData()


def load():
    name_list = []

    doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir('../symptom')
                   if os.path.isfile(os.path.join('../symptom', docID_string))]
    doc_id_list.sort()
    for doc_id in doc_id_list:
        with open('../symptom/' + str(doc_id) + '.txt', encoding='utf8') as input_file:
            content = input_file.read()
            tokens = content.split(':')
            name = tokens[0]
            name_list.append(name)

        with open('../symptom/vectorized/' + str(doc_id) + '.txt', 'w', encoding='utf8') as output_file:
            output_file.write(name)

    with open('disease_name.txt', 'w', encoding='utf8') as classify_file:
        classify_file.write(json.dumps(name_list))


def classify_disease():
    mapping = collections.defaultdict(list)
    with open('disease_name.txt', encoding='utf8') as classify_file:
        diseases = json.load(classify_file)
        for i, disease in enumerate(diseases):
            if 'in situ' in disease:
                mapping['in situ'].append(i)
            elif 'tumor' in disease:
                mapping['tumor'].append(i)
            elif 'cataract' in disease:
                mapping['cataract'].append(i)
            elif 'influenza' in disease:
                mapping['influenza'].append(i)
            elif 'system disease' in disease:
                mapping['system disease'].append(i)
            elif 'sclerosis' in disease:
                mapping['sclerosis'].append(i)
            elif 'deafness' in disease:
                mapping['deafness'].append(i)
            elif 'metabolism disease' in disease or ('metabolism' in disease and 'disease' in disease):
                mapping['metabolism disease'].append(i)
            elif 'Charcot-Marie-Tooth' in disease:
                mapping['Charcot-Marie-Tooth'].append(i)
            elif 'Bartter' in disease:
                mapping['Bartter'].append(i)
            elif 'Gaucher' in disease:
                mapping['Gaucher'].append(i)
            elif 'Brugada' in disease:
                mapping['Brugada'].append(i)
            elif 'uterine corpus' in disease:
                mapping['uterine corpus'].append(i)
            elif 'Waardenburg' in disease:
                mapping['Waardenburg'].append(i)
            elif 'Trypanosoma' in disease:
                mapping['Trypanosoma'].append(i)
            elif 'metabolism disorder' in disease:
                mapping['metabolism disorder'].append(i)
            elif 'Fanconi' in disease:
                mapping['Fanconi'].append(i)
            elif 'cone-rod dystrophy' in disease:
                mapping['cone-rod dystrophy'].append(i)
            elif 'hemochromatosis' in disease:
                mapping['hemochromatosis'].append(i)
            elif 'familial hypocalciuric hypercalcemia' in disease:
                mapping['familial hypocalciuric hypercalcemia'].append(i)
            elif 'lymphoproliferative' in disease:
                mapping['lymphoproliferative'].append(i)
            elif 'CADASIL' in disease:
                mapping['CADASIL'].append(i)
            elif 'holoprosencephaly' in disease:
                mapping['holoprosencephaly'].append(i)
            elif 'night blindness' in disease:
                mapping['night blindness'].append(i)
            elif 'Oguchi' in disease:
                mapping['Oguchi'].append(i)
            elif 'Warburg' in disease:
                mapping['Warburg'].append(i)
            elif 'Mansonella' in disease:
                mapping['Mansonella'].append(i)
            elif 'cell breast carcinoma' in disease:
                mapping['cell breast carcinoma'].append(i)
            elif 'squamous breast carcinoma' in disease:
                mapping['squamous breast carcinoma'].append(i)
            elif 'papillomatosis' in disease:
                mapping['papillomatosis'].append(i)
            elif 'rhabdomyosarcoma' in disease:
                mapping['rhabdomyosarcoma'].append(i)
            elif 'retinitis pigmentosa' in disease:
                mapping['retinitis pigmentosa'].append(i)
            elif 'dilated cardiomyopathy' in disease:
                mapping['dilated cardiomyopathy'].append(i)
            elif 'hypermethioninemia' in disease:
                mapping['hypermethioninemia'].append(i)
            elif 'Trichomonas' in disease:
                mapping['Trichomonas'].append(i)
            elif 'Herpes ' in disease:
                mapping['Herpes'].append(i)
            elif 'Leber' in disease:
                mapping['Leber'].append(i)
            elif 'paracoccidioidomycosis' in disease:
                mapping['paracoccidioidomycosis'].append(i)
            elif 'sporotrichosis' in disease:
                mapping['sporotrichosis'].append(i)
            elif 'Axenfeld-Rieger' in disease:
                mapping['Axenfeld-Rieger'].append(i)
            elif 'Bardet-Biedl' in disease:
                mapping['Bardet-Biedl'].append(i)
            elif 'atrial heart' in disease:
                mapping['atrial heart'].append(i)
            elif 'mucormycosis' in disease:
                mapping['mucormycosis'].append(i)
            elif 'atopic dermatitis' in disease:
                mapping['atopic dermatitis'].append(i)
            elif 'trichostrongyl' in disease:
                mapping['trichostrongyl'].append(i)
            elif 'spotted fever' in disease:
                mapping['spotted fever'].append(i)
            elif 'tick typhus' in disease:
                mapping['tick typhus'].append(i)
            elif 'sinusitis' in disease:
                mapping['sinusitis'].append(i)
            elif 'endometritis' in disease:
                mapping['endometritis'].append(i)
            elif 'Trichophyton' in disease:
                mapping['Trichophyton'].append(i)
            elif 'Microsporum' in disease:
                mapping['Microsporum'].append(i)
            elif 'hypertrophic' in disease:
                mapping['hypertrophic'].append(i)
            elif 'dominant limb-girdle' in disease:
                mapping['dominant limb-girdle'].append(i)
            elif 'adenoma' in disease:
                mapping['adenoma'].append(i)
            elif 'teratoma' in disease:
                mapping['teratoma'].append(i)
            elif 'lung' in disease and 'carcinoma' in disease:
                mapping['lung carcinoma'].append(i)
            elif 'carcinosarcoma' in disease:
                mapping['carcinosarcoma'].append(i)
            elif 'hemorrhagic fever' in disease:
                mapping['hemorrhagic fever'].append(i)
            elif 'gland disease' in disease:
                mapping['gland disease'].append(i)
            elif 'encephalitis' in disease:
                mapping['encephalitis'].append(i)
            elif 'interstitial pneumonia' in disease:
                mapping['interstitial pneumonia'].append(i)
            elif 'Bartholin' in disease:
                mapping['Bartholin'].append(i)
            elif 'Angiostrongylus' in disease:
                mapping['Angiostrongylus'].append(i)
            elif 'infectious disease' in disease:
                words = disease.split()
                valid = False

                for word in words:
                    if word.istitle():
                        valid = True
                        break

                if valid:
                    mapping['infectious disease'].append(i)
            elif 'capillariasis' in disease:
                mapping['capillariasis'].append(i)
            elif 'malignant neoplasm' in disease:
                mapping['malignant neoplasm'].append(i)
            elif 'Coxiella' in disease:
                mapping['Coxiella'].append(i)
            elif 'vaginal' is disease:
                if 'papilloma' in disease:
                    mapping['vaginal papilloma'].append(i)
                elif 'tumor' in disease:
                    mapping['vaginal tumor'].append(i)
            elif 'gastroenteritis' in disease:
                mapping['gastroenteritis'].append(i)
            elif 'hepatitis' in disease:
                mapping['hepatitis'].append(i)
            elif 'adenocarcinoma' in disease:
                mapping['adenocarcinoma'].append(i)
            elif 'myotonic' in disease:
                mapping['myotonic'].append(i)
            elif 'pigmentary retinopathy' in disease:
                mapping['pigmentary retinopathy'].append(i)
            elif 'syphilis' in disease:
                mapping['syphilis'].append(i)
            elif 'tuberculosis' in disease:
                mapping['tuberculosis'].append(i)
            elif 'cystadenocarcinoma' in disease:
                mapping['cystadenocarcinoma'].append(i)
            elif 'adenocarcinoma' in disease:
                mapping['adenocarcinoma'].append(i)
            elif 'carcinoma' in disease:
                mapping['carcinoma'].append(i)
            elif 'bone' in disease and 'disease' in disease:
                mapping['bone disease'].append(i)
            elif 'acromesomelic' in disease:
                mapping['acromesomelic'].append(i)
            elif 'chondrodysplasia' in disease:
                mapping['chondrodysplasia'].append(i)
            elif 'dysplasia' in disease:
                mapping['dysplasia'].append(i)
            elif 'osteomalacia' in disease:
                mapping['osteomalacia'].append(i)
            elif 'hyperostosis' in disease:
                mapping['hyperostosis'].append(i)
            elif 'trichorhinophalangeal' in disease:
                mapping['trichorhinophalangeal'].append(i)
            elif 'achondrogenesis' in disease:
                mapping['achondrogenesis'].append(i)
            elif 'meningitis' in disease:
                mapping['meningitis'].append(i)
            elif 'gastritis' in disease:
                mapping['gastritis'].append(i)
            elif 'osteogenesis imperfecta' in disease:
                mapping['osteogenesis imperfecta'].append(i)
            elif 'myofibrillar myopathy' in disease:
                mapping['myofibrillar myopathy'].append(i)
            elif 'hypertrophic cardiomyopathy' in disease:
                mapping['hypertrophic cardiomyopathy'].append(i)
            elif 'cardiomyopathy' in disease:
                mapping['cardiomyopathy'].append(i)
            elif 'mycosis' in disease:
                if 'superficial' in disease:
                    mapping['superficial mycosis'].append(i)
                elif 'opportunistic' in disease:
                    mapping['opportunistic mycosis'].append(i)
                elif 'cutaneous' in disease:
                    mapping['cutaneous mycosis'].append(i)
                else:
                    mapping['mycosis'].append(i)
            elif 'botulism' in disease:
                mapping['botulism'].append(i)
            elif 'hordeolum' in disease:
                mapping['hordeolum'].append(i)
            elif ' leukodystrophy' in disease:
                mapping['leukodystrophy'].append(i)
            elif 'pre-eclampsia' in disease:
                mapping['pre-eclampsia'].append(i)
            elif 'CAKUT' in disease:
                mapping['CAKUT'].append(i)
            elif 'yaws' in disease:
                mapping['yaws'].append(i)
            elif 'dependence' in disease:
                mapping['dependence'].append(i)
            elif 'polycystic kidney' in disease:
                mapping['polycystic kidney'].append(i)
            elif 'microcephaly' in disease or 'chorioretinopathy' in disease:
                mapping['microcephaly and chorioretinopathy'].append(i)
            elif 'mitochondrial complex' in disease:
                mapping['mitochondrial complex'].append(i)
            elif 'lymphocytic' in disease or 'lymphoblastic' in disease or 'leukemia' in disease:
                mapping['leukemia'].append(i)
            elif 'keratitis' in disease:
                mapping['keratitis'].append(i)
            elif 'acyl-CoA' in disease:
                mapping['acyl-CoA'].append(i)
            elif 'benign neoplasm' in disease:
                mapping['benign neoplasm'].append(i)
            elif 'neoplasms' in disease:
                mapping['neoplasms'].append(i)
            elif 'neoplasm' in disease:
                mapping['neoplasm'].append(i)
            elif 'retinitis' in disease:
                mapping['retinitis'].append(i)
            elif 'candidiasis' in disease:
                mapping['candidiasis'].append(i)
            elif 'exstrophy' in disease:
                mapping['exstrophy'].append(i)
            elif 'blepharoconjunctivitis' in disease:
                mapping['blepharoconjunctivitis'].append(i)
            elif 'tularemia' in disease:
                mapping['tularemia'].append(i)
            elif 'struma ovarii' in disease:
                mapping['struma ovarii'].append(i)
            elif 'osteosarcoma' in disease:
                mapping['osteosarcoma'].append(i)
            elif 'colitis' in disease:
                mapping['colitis'].append(i)
            elif 'enterocolitis' in disease:
                mapping['enterocolitis'].append(i)
            elif 'yolk sac tumor' in disease:
                mapping['yolk sac tumor'].append(i)
            elif 'leiomyosarcoma' in disease:
                mapping['leiomyosarcoma'].append(i)
            elif 'diphtheria' in disease:
                mapping['diphtheria'].append(i)
            elif 'myocarditis' in disease:
                mapping['myocarditis'].append(i)
            elif 'keratoconjunctivitis' in disease:
                mapping['keratoconjunctivitis'].append(i)
            elif 'conjunctivitis' in disease:
                mapping['conjunctivitis'].append(i)
            elif 'congenital ichthyosis' in disease:
                mapping['congenital ichthyosis'].append(i)
            elif 'epidermolysis bullosa' in disease:
                mapping['epidermolysis bullosa'].append(i)
            elif 'adenomyoepithelioma' in disease:
                mapping['adenomyoepithelioma'].append(i)
            elif 'platelet-type bleeding' in disease:
                mapping['platelet-type bleeding'].append(i)
            elif 'Bernard-Soulier' in disease:
                mapping['Bernard-Soulier'].append(i)
            elif 'quadrant cancer' in disease:
                mapping['quadrant cancer'].append(i)
            elif 'glycogen storage' in disease:
                mapping['glycogen storage'].append(i)
            elif 'neonatal diabetes' in disease:
                mapping['neonatal diabetes'].append(i)
            elif 'cerebral cavernous' in disease:
                mapping['cerebral cavernous'].append(i)
            elif 'catecholaminergic' in disease:
                mapping['catecholaminergic'].append(i)
            elif 'Seckel' in disease:
                mapping['Seckel'].append(i)
            elif 'leiomyoma' in disease:
                mapping['leiomyoma'].append(i)
            elif 'lobe epilepsy' in disease:
                mapping['lobe epilepsy'].append(i)
            elif 'dyskeratosis congenita' in disease:
                mapping['dyskeratosis congenita'].append(i)
            elif 'progressive familial' in disease:
                mapping['progressive familial'].append(i)
            elif 'auditory neuropathy' in disease:
                mapping['auditory neuropathy'].append(i)
            elif 'synthesis defect' in disease:
                mapping['synthesis defect'].append(i)
            elif 'hypobetalipoproteinemia' in disease:
                mapping['hypobetalipoproteinemia'].append(i)
            elif 'hyperekplexia' in disease:
                mapping['hyperekplexia'].append(i)
            elif 'calcinosis' in disease:
                mapping['calcinosis'].append(i)
            elif 'muscular atrophy' in disease:
                mapping['muscular atrophy'].append(i)
            elif 'amyloid angiopathy' in disease:
                mapping['amyloid angiopathy'].append(i)
            elif 'intellectual disability' in disease:
                mapping['intellectual disability'].append(i)
            elif 'Coffin-Siris' in disease:
                mapping['Coffin-Siris'].append(i)
            elif 'mental retardation' in disease:
                mapping['mental retardation'].append(i)
            elif 'hemangiopericytoma' in disease:
                mapping['hemangiopericytoma'].append(i)
            elif 'Joubert' in disease:
                mapping['Joubert'].append(i)
            elif 'schizophrenia' in disease:
                mapping['schizophrenia'].append(i)
            elif 'pneumonia' in disease:
                mapping['pneumonia'].append(i)
            elif 'endometriosis' in disease:
                mapping['endometriosis'].append(i)
            elif 'oculocutaneous albinism' in disease:
                mapping['oculocutaneous albinism'].append(i)
            elif 'perichondritis' in disease:
                mapping['perichondritis'].append(i)
            elif 'alveolar' in disease:
                mapping['alveolar'].append(i)
            elif 'sinus tumor' in disease:
                mapping['sinus tumor'].append(i)
            elif 'amnesia' in disease:
                mapping['amnesia'].append(i)
            elif 'hemangioma' in disease:
                mapping['hemangioma'].append(i)
            elif 'exostosis' in disease:
                mapping['exostosis'].append(i)
            elif 'valve' in disease:
                mapping['valve'].append(i)
            elif 'encephalopathy' in disease:
                mapping['encephalopathy'].append(i)
            elif 'aciduria' in disease:
                mapping['aciduria'].append(i)
            elif 'epilepsy' in disease:
                mapping['epilepsy'].append(i)
            elif 'agnosia' in disease:
                mapping['agnosia'].append(i)
            elif 'hyperplasia' in disease:
                mapping['hyperplasia'].append(i)
            elif 'acidemia' in disease:
                mapping['acidemia'].append(i)
            elif 'Griscelli' in disease:
                mapping['Griscelli'].append(i)
            elif 'microphthalmia' in disease:
                mapping['microphthalmia'].append(i)
            elif 'neuropathy' in disease:
                mapping['neuropathy'].append(i)
            elif 'hemangiopericytoma' in disease:
                mapping['hemangiopericytoma'].append(i)
            elif 'pancreas' in disease:
                mapping['pancreas'].append(i)
            elif 'pseudohypoaldosteronism' in disease:
                mapping['pseudohypoaldosteronism'].append(i)
            elif 'methylmalonic acidemia' in disease:
                mapping['methylmalonic acidemia'].append(i)
            elif 'macular dystrophy' in disease:
                mapping['macular dystrophy'].append(i)
            elif 'melanoma' in disease:
                mapping['melanoma'].append(i)
            elif 'stenosis' in disease:
                mapping['stenosis'].append(i)
            elif 'anthrax' in disease:
                mapping['anthrax'].append(i)
            elif 'immunodeficiency' in disease:
                mapping['immunodeficiency'].append(i)
            elif 'sclerosteosis' in disease:
                mapping['sclerosteosis'].append(i)
            elif 'molybdenum cofactor' in disease:
                mapping['molybdenum cofactor'].append(i)
            elif 'histoplasmosis' in disease:
                mapping['histoplasmosis'].append(i)
            elif 'pericarditis' in disease:
                mapping['pericarditis'].append(i)
            elif 'mesothelioma' in disease:
                mapping['mesothelioma'].append(i)
            elif 'Robinow' in disease:
                mapping['Robinow'].append(i)
            elif 'tachycardia' in disease:
                mapping['tachycardia'].append(i)
            elif 'spinocerebellar ataxia' in disease:
                mapping['spinocerebellar ataxia'].append(i)
            elif 'dextro-looped transposition' in disease:
                mapping['dextro-looped transposition'].append(i)
            elif 'congenital' in disease and 'diarrhea' in disease:
                mapping['congenital diarrhea'].append(i)
            elif 'lymphoma' in disease:
                mapping['lymphoma'].append(i)
            elif 'ectopia lentis' in disease:
                mapping['ectopia lentis'].append(i)
            elif 'Noonan syndrome' in disease:
                mapping['Noonan syndrome'].append(i)
            elif 'toxoplasmosis' in disease:
                mapping['toxoplasmosis'].append(i)
            elif 'Niemann-Pick disease' in disease:
                mapping['Niemann-Pick disease'].append(i)
            elif 'Meckel' in disease:
                mapping['Meckel'].append(i)
            elif 'hypothyroidism' in disease:
                mapping['hypothyroidism'].append(i)
            elif 'cutis' in disease:
                mapping['cutis'].append(i)
            elif 'peritonitis' in disease:
                mapping['peritonitis'].append(i)
            elif 'bone cancer' in disease:
                mapping['bone cancer'].append(i)
            elif 'lipodystrophy' in disease:
                mapping['lipodystrophy'].append(i)
            elif 'nephronophthisis' in disease:
                mapping['nephronophthisis'].append(i)
            elif 'maturity-onset diabetes' in disease:
                mapping['maturity-onset diabetes'].append(i)
            elif 'anemia' in disease:
                mapping['anemia'].append(i)
            elif 'dermatitis' in disease:
                mapping['dermatitis'].append(i)
            elif 'phobia' in disease:
                mapping['phobia'].append(i)
            elif 'chordoma' in disease:
                mapping['chordoma'].append(i)
            elif 'leprosy' in disease:
                mapping['leprosy'].append(i)
            elif 'pneumonitis' in disease:
                mapping['pneumonitis'].append(i)
            elif 'iridocyclitis' in disease:
                mapping['iridocyclitis'].append(i)
            elif 'leukoplakia' in disease:
                mapping['leukoplakia'].append(i)
            elif 'Parkinson' in disease:
                mapping['Parkinson'].append(i)
            elif 'thrombosis' in disease:
                mapping['thrombosis'].append(i)
            elif 'migraine' in disease:
                mapping['migraine'].append(i)
            elif 'nephritis' in disease:
                mapping['nephritis'].append(i)
            elif 'abuse' in disease:
                mapping['abuse'].append(i)
            elif 'arteritis' in disease:
                mapping['arteritis'].append(i)
            elif 'adamantinoma' in disease:
                mapping['adamantinoma'].append(i)
            elif 'leukoencephalopathy' in disease:
                mapping['leukoencephalopathy'].append(i)
            elif 'hormone deficiency' in disease:
                mapping['hormone deficiency'].append(i)
            elif 'cryptococcosis' in disease:
                mapping['cryptococcosis'].append(i)
            elif 'hypomagnesemia' in disease:
                mapping['hypomagnesemia'].append(i)
            elif 'anxiety' in disease:
                mapping['anxiety'].append(i)
            elif 'malaria' in disease:
                mapping['malaria'].append(i)
            elif 'empyema' in disease:
                mapping['empyema'].append(i)
            elif 'pleuropneumonia' in disease:
                mapping['pleuropneumonia'].append(i)
            elif 'echinococcosis' in disease:
                mapping['echinococcosis'].append(i)
            elif 'tic disorder' in disease:
                mapping['tic disorder'].append(i)
            elif 'labyrinthitis' in disease:
                mapping['labyrinthitis'].append(i)
            elif 'relapsing fever' in disease:
                mapping['relapsing fever'].append(i)
            elif 'ancylostomiasis' in disease:
                mapping['ancylostomiasis'].append(i)
            elif 'retinoblastoma' in disease:
                mapping['retinoblastoma'].append(i)
            elif 'ovarian' in disease and 'papil' in disease:
                mapping['ovarian disease'].append(i)
            elif 'esophagitis' in disease:
                mapping['esophagitis'].append(i)
            elif 'poliovirus' in disease:
                mapping['poliovirus'].append(i)
            elif 'spastic' in disease:
                mapping['spastic'].append(i)
            elif 'neuromuscular' in disease:
                mapping['neuromuscular'].append(i)
            elif 'oophoritis' in disease:
                mapping['oophoritis'].append(i)
            elif 'chondrosarcoma' in disease:
                mapping['chondrosarcoma'].append(i)
            elif 'paronychia' in disease:
                mapping['paronychia'].append(i)
            elif 'scleroderma' in disease:
                mapping['scleroderma'].append(i)
            elif 'ovar' in disease and 'cyst' in disease:
                mapping['overian cyst'].append(i)
            elif 'adenofibroma' in disease:
                mapping['adenofibroma'].append(i)
            elif 'leiomyomatosis' in disease:
                mapping['leiomyomatosis'].append(i)
            elif 'cystadenofibroma' in disease:
                mapping['cystadenofibroma'].append(i)
            elif 'thoracic dystrophy' in disease:
                mapping['thoracic dystrophy'].append(i)
            elif 'allergic asthma' in disease:
                mapping['allergic asthma'].append(i)
            elif 'personality disorder' in disease:
                mapping['personality disorder'].append(i)
            elif 'cervicitis' in disease:
                mapping['cervicitis'].append(i)
            elif 'fibrosis' in disease:
                mapping['fibrosis'].append(i)
            elif 'cystitis' in disease:
                mapping['cystitis'].append(i)
            elif 'amelogenesis imperfecta' in disease:
                mapping['amelogenesis imperfecta'].append(i)
            elif 'urticaria' in disease:
                mapping['urticaria'].append(i)
            elif 'dysgenesis' in disease:
                mapping['dysgenesis'].append(i)
            elif 'Alzheimer' in disease:
                mapping['Alzheimer'].append(i)
            elif 'Brucella' in disease:
                mapping['Brucella'].append(i)
            elif 'achromatopsia' in disease:
                mapping['achromatopsia'].append(i)
            elif '3-methylglutaconic aciduria' in disease:
                mapping['3-methylglutaconic aciduria'].append(i)
            elif 'strongyloidiasis' in disease:
                mapping['strongyloidiasis'].append(i)
            elif 'arthropathy' in disease:
                mapping['arthropathy'].append(i)
            elif 'Alport syndrome' in disease:
                mapping['Alport syndrome'].append(i)
            elif 'endocarditis' in disease:
                mapping['endocarditis'].append(i)
            elif 'macular degeneration' in disease:
                mapping['macular degeneration'].append(i)
            elif 'aspergillosis' in disease:
                mapping['aspergillosis'].append(i)
            elif 'sleep phase syndrome' in disease:
                mapping['sleep phase syndrome'].append(i)
            elif 'allergy' in disease or 'allergic' in disease:
                mapping['allergy'].append(i)
            elif 'liposarcoma' in disease:
                mapping['liposarcoma'].append(i)
            elif 'medulloblastoma' in disease:
                mapping['medulloblastoma'].append(i)
            elif 'ameloblastoma' in disease:
                mapping['ameloblastoma'].append(i)
            elif 'pheochromocytoma' in disease:
                mapping['pheochromocytoma'].append(i)
            elif 'system cancer' in disease:
                mapping['system cancer'].append(i)
            elif 'dystonia' in disease:
                mapping['dystonia'].append(i)
            elif 'apnea' in disease:
                mapping['apnea'].append(i)
            elif 'hypocalcemia' in disease:
                mapping['hypocalcemia'].append(i)
            elif 'hypercholesterolemia' in disease:
                mapping['hypercholesterolemia'].append(i)
            elif 'Huntington' in disease:
                mapping['Huntington'].append(i)
            elif 'lipoma' in disease:
                mapping['lipoma'].append(i)
            elif 'metabolic disorder' in disease:
                mapping['metabolic disorder'].append(i)
            elif 'astrocytoma' in disease:
                mapping['astrocytoma'].append(i)
            elif 'neuroblastoma' in disease:
                mapping['neuroblastoma'].append(i)
            elif 'brucellosis' in disease:
                mapping['brucellosis'].append(i)
            elif 'infestation' in disease:
                mapping['infestation'].append(i)
            elif 'hand-foot malformation' in disease:
                mapping['hand-foot malformation'].append(i)
            elif 'leishmaniasis' in disease:
                mapping['leishmaniasis'].append(i)
            elif 'dysentery' in disease:
                mapping['dysentery'].append(i)
            elif 'pasteurellosis' in disease:
                mapping['pasteurellosis'].append(i)
            elif 'oligodendroglioma' in disease:
                mapping['oligodendroglioma'].append(i)
            elif 'ependymoma' in disease:
                mapping['ependymoma'].append(i)
            elif 'nevus' in disease:
                mapping['nevus'].append(i)
            elif 'dyskinesia' in disease:
                mapping['dyskinesia'].append(i)
            elif 'deficiency syndrome' in disease:
                mapping['deficiency syndrome'].append(i)
            elif 'hypogonadotropic hypogonadism' in disease or 'hypogonadism' in disease:
                mapping['hypogonadism'].append(i)
            elif 'autoinflammatory syndrome' in disease:
                mapping['autoinflammatory syndrome'].append(i)
            elif 'FTDALS' in disease:
                mapping['FTDALS'].append(i)
            elif 'Cogan' in disease:
                mapping['Cogan'].append(i)
            elif 'dysostosis' in disease:
                mapping['dysostosis'].append(i)
            elif 'fibrillation' in disease:
                mapping['fibrillation'].append(i)
            elif 'aneurysm' in disease:
                mapping['aneurysm'].append(i)
            elif 'retinoblastoma' in disease:
                mapping['retinoblastoma'].append(i)
            elif 'carnitine palmitoyltransferase' in disease:
                mapping['carnitine palmitoyltransferase'].append(i)
            elif 'sclerosteosis' in disease:
                mapping['sclerosteosis'].append(i)
            elif 'myopathy' in disease:
                mapping['myopathy'].append(i)
            elif 'hereditaria' in disease:
                mapping['hereditaria'].append(i)
            elif 'ptosis' in disease:
                mapping['ptosis'].append(i)
            elif 'amebiasis' in disease:
                mapping['amebiasis'].append(i)
            elif 'cutis laxa' in disease:
                mapping['cutis laxa'].append(i)
            elif 'encephalomyelitis' in disease:
                mapping['encephalomyelitis'].append(i)
            elif 'cerebellar ataxia' in disease:
                mapping['cerebellar ataxia'].append(i)
            elif 'hypophosphatemic rickets' in disease:
                mapping['hypophosphatemic rickets'].append(i)
            elif 'reductase deficiency' in disease:
                mapping['reductase deficiency'].append(i)
            elif 'cystinuria' in disease:
                mapping['cystinuria'].append(i)
            elif 'condylomata acuminata' in disease:
                mapping['condylomata acuminata'].append(i)
            elif 'otitis media' in disease:
                mapping['otitis media'].append(i)
            elif 'cheilitis' in disease:
                mapping['cheilitis'].append(i)
            elif 'hypertrophy' in disease:
                mapping['hypertrophy'].append(i)
            elif 'orofaciodigital' in disease:
                mapping['orofaciodigital'].append(i)
            elif 'wart' in disease:
                mapping['wart'].append(i)
            elif 'rubella' in disease:
                mapping['rubella'].append(i)
            elif 'hernia' in disease:
                mapping['hernia'].append(i)
            elif 'abscess' in disease:
                mapping['abscess'].append(i)
            elif 'mellitus' in disease:
                mapping['mellitus'].append(i)
            elif 'cystadenoma' in disease:
                mapping['cystadenoma'].append(i)
            elif 'angiomatosis' in disease:
                mapping['angiomatosis'].append(i)
            elif 'leiomyomatosis' in disease:
                mapping['leiomyomatosis'].append(i)
            elif 'tyrosinemia' in disease:
                mapping['tyrosinemia'].append(i)
            elif 'hypertension' in disease:
                mapping['hypertension'].append(i)
            elif 'adenosarcoma' in disease:
                mapping['adenosarcoma'].append(i)
            elif 'alopecia' in disease:
                mapping['alopecia'].append(i)
            elif 'osteopetrosis' in disease:
                mapping['osteopetrosis'].append(i)
            elif 'filariasis' in disease:
                mapping['filariasis'].append(i)
            elif 'dementia' in disease:
                mapping['dementia'].append(i)
            elif 'trichomoniasis' in disease:
                mapping['trichomoniasis'].append(i)
            elif 'cholecystitis' in disease:
                mapping['cholecystitis'].append(i)
            elif 'sarcoma' in disease:
                mapping['sarcoma'].append(i)
            elif 'mutism' in disease:
                mapping['mutism'].append(i)
            elif 'arthritis' in disease:
                mapping['arthritis'].append(i)
            elif 'ankyloglossia' in disease:
                mapping['ankyloglossia'].append(i)
            elif 'primordial dwarfism' in disease:
                mapping['primordial dwarfism'].append(i)
            elif 'salpingitis' in disease:
                mapping['salpingitis'].append(i)
            elif 'Hermansky-Pudlak' in disease:
                mapping['Hermansky-Pudlak'].append(i)
            elif 'leiomyosarcoma' in disease:
                mapping['leiomyosarcoma'].append(i)
            elif 'contracture syndrome' in disease:
                mapping['contracture syndrome'].append(i)
            elif 'von Willebrand' in disease:
                mapping['von Willebrand'].append(i)
            elif 'Ritscher-Schinzel syndrome' in disease:
                mapping['Ritscher-Schinzel syndrome'].append(i)
            elif '3MC syndrome' in disease:
                mapping['3MC syndrome'].append(i)
            elif 'QT syndrome' in disease:
                mapping['QT syndrome'].append(i)
            elif 'muscular dystrophy' in disease:
                mapping['muscular dystrophy'].append(i)
            elif 'ciliary dyskinesia' in disease:
                mapping['ciliary dyskinesia'].append(i)
            elif 'mucopolysaccharidosis' in disease:
                mapping['mucopolysaccharidosis'].append(i)
            elif 'myasthenic syndrome' in disease:
                mapping['myasthenic syndrome'].append(i)
            elif 'endocrine neoplasia' in disease:
                mapping['endocrine neoplasia'].append(i)
            elif 'ehrlichiosis' in disease:
                mapping['ehrlichiosis'].append(i)
            elif 'breast papilloma' in disease:
                mapping['breast papilloma'].append(i)
            elif 'lipoma' in disease:
                mapping['lipoma'].append(i)
            elif 'nephroma' in disease:
                mapping['nephroma'].append(i)
            elif 'blastoma' in disease:
                mapping['blastoma'].append(i)
            elif 'spastic paraplegia' in disease:
                mapping['spastic paraplegia'].append(i)
            elif 'diabetes mellitus' in disease:
                mapping['diabetes mellitus'].append(i)
            elif 'neurodegeneration' in disease:
                mapping['neurodegeneration'].append(i)
            elif 'ceroid lipofuscinosis' in disease:
                mapping['ceroid lipofuscinosis'].append(i)
            elif 'hydroxyglutaric aciduria' in disease:
                mapping['hydroxyglutaric aciduria'].append(i)
            elif 'disorder of glycosylation' in disease:
                mapping['disorder of glycosylation'].append(i)
            elif 'plague' in disease:
                mapping['plague'].append(i)
            elif 'poliomyelitis' in disease:
                mapping['poliomyelitis'].append(i)
            elif 'onchocerciasis' in disease:
                mapping['onchocerciasis'].append(i)
            elif 'hypotrichosis' in disease:
                mapping['hypotrichosis'].append(i)
            elif 'color blindness' in disease:
                mapping['color blindness'].append(i)
            elif 'xanthomatosis' in disease:
                mapping['xanthomatosis'].append(i)
            elif 'inflammatory bowel disease' in disease:
                mapping['inflammatory bowel disease'].append(i)
            elif 'ovarian cystadenoma' in disease:
                mapping['ovarian cystadenoma'].append(i)
            elif 'breast cancer' in disease or ('breast' in disease and 'cancer' in disease):
                mapping['breast cancer'].append(i)
            elif 'corneal dystrophy' in disease:
                mapping['corneal dystrophy'].append(i)
            elif 'pigmentosum' in disease:
                mapping['pigmentosum'].append(i)
            elif 'cerebral palsy' in disease:
                mapping['cerebral palsy'].append(i)
            elif 'alcohol syndrome' in disease:
                mapping['alcohol syndrome'].append(i)
            elif 'xeroderma pigmentosum' in disease:
                mapping['xeroderma pigmentosum'].append(i)
            elif 'Usher syndrome' in disease:
                mapping['Usher syndrome'].append(i)
            elif 'angiosarcoma' in disease:
                mapping['angiosarcoma'].append(i)
            elif 'autoimmune disease' in disease:
                mapping['autoimmune disease'].append(i)
            elif 'endometrial stromal' in disease:
                mapping['endometrial stromal'].append(i)
            elif 'recombinase activating gene' in disease:
                mapping['recombinase activating gene'].append(i)
            elif 'brachydactyly' in disease:
                mapping['brachydactyly'].append(i)
            elif 'kyphosis' in disease:
                mapping['kyphosis'].append(i)
            elif 'hemophagocytic lymphohistiocytosis' in disease:
                mapping['hemophagocytic lymphohistiocytosis'].append(i)
            elif 'nemaline myopathy' in disease:
                mapping['nemaline myopathy'].append(i)
            elif 'hypophosphatasia' in disease:
                mapping['hypophosphatasia'].append(i)
            elif 'spherocytosis' in disease:
                mapping['spherocytosis'].append(i)
            elif 'mucopolysaccharidosis' in disease:
                mapping['mucopolysaccharidosis'].append(i)
            elif 'endometriosis' in disease:
                mapping['endometriosis'].append(i)
            elif 'Kaposi' in disease:
                mapping['Kaposi sarcoma'].append(i)
            elif 'cervical adenomyoma' in disease:
                mapping['cervical adenomyoma'].append(i)
            elif 'chain disease' in disease:
                mapping['chain disease'].append(i)
            elif 'cystocele' in disease:
                mapping['cystocele'].append(i)
            elif 'diarrhea' in disease:
                mapping['diarrhea'].append(i)
            elif 'shock syndrome' in disease:
                mapping['shock syndrome'].append(i)
            elif 'vaginal' in disease and 'papilloma' in disease:
                mapping['vaginal papilloma'].append(i)
            elif 'Wolfram syndrome' in disease:
                mapping['Wolfram syndrome'].append(i)
            elif 'typhus' in disease:
                mapping['typhus'].append(i)
            elif 'synostosis' in disease:
                mapping['synostosis'].append(i)
            elif 'chondroma' in disease:
                mapping['chondroma'].append(i)
            elif 'adenomyoma' in disease:
                mapping['adenomyoma'].append(i)
            elif 'herpes zoster' in disease:
                mapping['herpes zoster'].append(i)
            elif 'osteonecrosis' in disease:
                mapping['osteonecrosis'].append(i)
            elif 'glioma' in disease:
                mapping['glioma'].append(i)
            elif 'papilloma' in disease:
                mapping['papilloma'].append(i)
            elif 'autosomal' in disease and 'disease' in disease:
                mapping['autosomal disease'].append(i)
            elif 'amyloidosis' in disease:
                mapping['amyloidosis'].append(i)
            elif 'rhinitis' in disease:
                mapping['rhinitis'].append(i)
            elif 'vasculitis' in disease:
                mapping['vasculitis'].append(i)
            elif 'muscular disease' in disease:
                mapping['muscular disease'].append(i)
            elif 'scoliosis' in disease:
                mapping['scoliosis'].append(i)
            elif 'inflammatory disease' in disease:
                mapping['inflammatory disease'].append(i)
            elif 'vasculitis' in disease:
                mapping['vasculitis'].append(i)
            elif 'vasculitis' in disease:
                mapping['vasculitis'].append(i)
            elif 'vasculitis' in disease:
                mapping['vasculitis'].append(i)
            else:
                mapping[disease].append(i)

    filtered_mapping = {}
    for key in mapping.keys():
        if 'deficiency' in key and (has_numbers(key) or has_short_form(key)):
            continue

        if 'syndrome' in key and (has_short_form(key) or has_name(key)):
            continue

        if 'disease' in key and (has_short_form(key) or has_name(key)):
            continue

        if 'cancer' in key or 'disorder' in key or \
                'AIDS' in key or "HIV" in key or \
                'acyl-CoA' in key or \
                'VACTERL' in key or \
                "'" in key or \
                'I' in key or 'V' in key or 'X' in key or 'll' in key:
            continue

        if key == 'syndrome':
            continue

        if len(key.split()) == 2 and (has_short_form(key) or has_numbers(key)):
            continue

        if has_symbol(key) or has_short_form(key) or has_numbers(key):
            continue

        if len(mapping[key]) > 10:
            filtered_mapping[key] = mapping[key]

    unique_doc_list = set()
    for key in filtered_mapping.keys():
        for doc_id in filtered_mapping[key]:
            unique_doc_list.add(doc_id)

    word_index = collections.defaultdict(int)
    index = 0
    for doc in unique_doc_list:
        with open('../symptom/' + str(doc) + '.txt', encoding='utf8') as input_file:
            content = input_file.read()
            tokens = content.split(':')
            description = tokens[1]
            words = nltk.word_tokenize(description)
            words = [word for word in words if len(word) > 1]

            for word in words:
                if word not in word_index.keys():
                    word_index[word] = index
                    index += 1

        vectorized = []
        with open('../symptom/vectorized/' + str(doc) + '.txt', 'a', encoding='utf8') as output_file:
            for word in words:
                vectorized.append(word_index[word])
            output_file.write(':' + json.dumps(vectorized))

    with open('word_index.txt', 'w', encoding='utf8') as mapping_file:
        mapping_file.write(json.dumps(word_index))

    with open('disease_label.txt', 'w', encoding='utf8') as label_file:
        label_file.write(json.dumps(filtered_mapping))


def link_mapping():
    with open('disease_label.txt', encoding='utf8') as input_file:
        label_dict = json.load(input_file)
        for i, label in enumerate(label_dict):
            doc_list = label_dict[label]
            for doc in doc_list:
                with open('../symptom/vectorized/' + str(doc) + '.txt', 'a', encoding='utf8') as output_file:
                    output_file.write(':' + str(i))


def prepare_data():
    train_data = []
    train_label = []

    doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir('../symptom/vectorized')]
    doc_id_list.sort()
    for doc_id in doc_id_list:
        with open('../symptom/vectorized/' + str(doc_id) + '.txt', encoding='utf8') as input_file:
            content = input_file.read()
            tokens = content.split(':')
            if len(tokens) == 3:
                train_data.append(json.loads(tokens[1]))
                train_label.append(int(tokens[2]))

    print('Finished preparing data')

    x_train = vectorize_sequences(train_data)
    one_hot_train_labels = to_one_hot(train_label)

    model = models.Sequential()
    model.add(layers.Dense(256, activation='relu', input_shape=(5900,)))
    model.add(layers.Dropout(0.35))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dropout(0.35))
    model.add(layers.Dense(85, activation='softmax'))
    model.compile(optimizer='rmsprop',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    x_val = x_train[:500]
    partial_x_train = x_train[500:]

    y_val = one_hot_train_labels[:500]
    partial_y_train = one_hot_train_labels[500:]

    print('Start to train model')

    checkpoint = ModelCheckpoint(
        'best.h5',
        monitor='val_loss',
        verbose=0,
        save_best_only=True,
        save_weights_only=False,
        mode='auto',
        period=1)
    history = model.fit(partial_x_train,
                        partial_y_train,
                        epochs=20,
                        batch_size=64,
                        callbacks=[checkpoint],
                        validation_data=(x_val, y_val))

    # model.save('my_model.h5')
    # del model

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(1, len(loss) + 1)

    plt.plot(epochs, loss, 'bo', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()

    plt.show()

    plt.clf()  # clear figure
    acc = history.history['acc']
    val_acc = history.history['val_acc']

    plt.plot(epochs, acc, 'bo', label='Training acc')
    plt.plot(epochs, val_acc, 'b', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()

    plt.show()


def use_model():
    test_data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 7, 11, 12, 13, 7, 14, 9, 15, 9, 16, 17]
    x_test = vectorize_sequences(test_data)
    model = load_model('my_model.h5')
    predictions = model.predict(x_test)
    for prediction in predictions:
        print(np.argmax(prediction))


def vectorize_sequences(sequences, dimension=5900):
    results = np.zeros((len(sequences), dimension))
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1.

    return results


def to_one_hot(labels, dimension=85):
    results = np.zeros((len(labels), dimension))
    for i, label in enumerate(labels):
        results[i, label] = 1.
    return results


def has_name(string):
    tokens = string.split()
    return any(char == '-' for char in tokens[0]) or tokens[0].istitle()


def has_short_form(string):
    tokens = string.split()
    return tokens[0].isupper()


def has_numbers(string):
    return any(char.isdigit() for char in string)


def has_symbol(string):
    return any(char == '_' or char == '/' for char in string)


def main():
    start = time.time()

    # load()
    # print('finished loading file')
    # classify_disease()
    # print('finished vectorization')
    # link_mapping()
    # print('finished mapping')
    prepare_data()

    # use_model()

    end = time.time()
    print('Process time: ' + str(end - start))


if __name__ == '__main__':
    main()
