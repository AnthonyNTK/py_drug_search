import re
import json
import time
import MetaData
import MapReduce
import Singular
import exception
from math import log
from collections import defaultdict


mr = MapReduce.MapReduce()
checker = Singular.Singular()

metadata = MetaData.MetaData()
metadata.set_disease_metadata()

with open(metadata.process_metadata[4], encoding='utf8') as doc_length_file:
    doc_length = json.load(doc_length_file)

upper_bound = 1.2                   # K-value in bm25 [need manual twist]
doc_length_modifier = 0.5           # B-value in bm25 [need manual twist]


def mapper(record):
    # record : [doc_id, doc_contents]
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    max_freq = 0

    word_freq = defaultdict(int)

    value = re.sub('/', ' ', value)
    words = value.split()

    # remove all symbols
    words = [re.sub(exception.symbol_exception, '', remove_hyphen(word.lower())) for word in words]

    # remove all unicode character/symbol
    words = [re.sub(exception.unicode_exception, '', word) for word in words]

    # remove empty string
    clean_word = list(filter(None, words))

    for word in clean_word:
        word_freq[word] = word_freq[word] + 1

    for word in clean_word:
        if word_freq[word] > max_freq:
            max_freq = word_freq[word]
        else:
            max_freq = max_freq

    # filter out all stopwords, numbers, symbols and useless words
    for word in clean_word:
        if word in metadata.stopwords:
            continue

        if is_number(word):
            continue

        if has_numbers(word):
            continue

        if has_strange_symbol(word):
            continue

        if len(word) < 3:
            continue

        # augmented frequency (raw frequency divided by the raw frequency of the most occurring term in the document)
        tf_norm = float(word_freq[word]) / float(max_freq)
        mr.emit_intermediate(word, [key, tf_norm])


def reducer(key, list_of_values):
    # key: term
    # value: [doc_id, normalized term frequency]
    index = []
    count = 0

    for doc in list_of_values:
        if not(doc in index):
            index.append(doc)
            count += 1

    # probabilistic inverse document frequency
    df = (float(2702) - float(count)) / float(count)
    idf = log(df, 2)

    for mapper_value in index:
        tf = mapper_value[1]

        # bm2.5+ algorithm
        doc_length_ratio = doc_length[str(mapper_value[0])] / metadata.process_metadata[5]
        tf = ((upper_bound + 1) * tf) / (
                upper_bound * (1 - doc_length_modifier + doc_length_modifier * doc_length_ratio) + tf
            ) + 1
        mapper_value[1] = tf * idf

    # output = [term, no of docs containing the term, tf-idf]
    mr.emit((key, count, index))


def remove_hyphen(word):
    return word.strip('-')


def is_number(token):
    try:
        float(token)
        return True
    except ValueError:
        return False


def has_numbers(input_string):
    return any(char.isdigit() for char in input_string)


def has_strange_symbol(input_string):
    return any(char == '+' for char in input_string)


def main():
    t0 = time.time()

    with open(metadata.process_metadata[1], encoding='utf8') as input_data:
        output = open(metadata.process_metadata[2], 'w', encoding='utf8')
        mr.execute(input_data, mapper, reducer, output)
        output.close()

    t1 = time.time()
    print('Processing time: ' + str(t1 - t0))


if __name__ == '__main__':
    main()
