import re
import json
import nltk
import time
import exception

valid_pos_list = ['NN', 'JJ', 'NNS', 'IN', 'VBG', 'DT']


def extract_by_pos(string):
    pos_tags = nltk.pos_tag(nltk.word_tokenize(string))

    index = 0
    count = 0
    result = []
    while count < len(pos_tags):
        if pos_tags[count][1] == 'JJ' or pos_tags[count][1] == 'NN':
            temp = []

            index += 1
            inner_count = count
            while inner_count < len(pos_tags):
                if pos_tags[inner_count][1] in valid_pos_list:
                    temp.append(pos_tags[inner_count][0])
                else:
                    result.append(temp)
                    count = inner_count + 1
                    break

                inner_count += 1

        count += 1

    valid_result = list(filter(lambda x: len(x) > 1, result))

    final_string = ''
    for valid in valid_result:
        temp_string = ' '.join(valid)
        final_string += temp_string + ','

    return final_string


def load():
    count = 0

    with open('doid.json', encoding='utf8') as json_file:
        diseases = json.load(json_file)
        for disease in diseases:
            try:
                print(count)
                disease_name = disease['lbl']
                disease_description = disease['meta']['definition']["val"]
                dirty_symptoms = re.search(exception.ontology_syndrome, disease_description)
                if dirty_symptoms is None:
                    clean_symptoms = extract_by_pos(disease_description)[:-1]
                else:
                    clean_symptoms = re.sub(exception.ontology_exception, '', dirty_symptoms.group(0))

                with open('../symptom/' + str(count) + '.txt', 'w', encoding='utf8') as output_file:
                    output_file.write(disease_name + ':' + clean_symptoms)

                with open('./mapping/' + str(count) + '.txt', 'w', encoding='utf8') as mapping_file:
                    mapping_file.write(json.dumps(disease))

                count += 1
            except KeyError:
                print(disease['id'])

        print(count)


def main():
    start = time.time()

    load()

    end = time.time()
    print('Process time: ' + str(end - start))


if __name__ == '__main__':
    main()
