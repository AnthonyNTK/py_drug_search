import numpy as np


def r_precision(r):
    r = np.asarray(r) != 0
    z = r.nonzero()[0]
    if not z.size:
        return 0.

    return np.mean(r[:z[-1] + 1])


def precision_at_k(r, k):
    assert k >= 1

    r = np.asarray(r)[:k] != 0
    if r.size != k:
        raise ValueError('Relevance score length < k')

    return np.mean(r)


def mean_x_precision(actual, k=10):
    if k is None:
        return np.mean([r_precision(a) for a in actual])

    return np.mean([precision_at_k(a, k) for a in actual])


def average_precision(actual, predicted, k):
    if len(predicted) > k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    for i, p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i+1.0)

    if not actual:
        return 0.0

    return score / min(len(actual), k)


def mean_average_precision(actual, predicted, k=10):
    return np.mean([average_precision(a, p, k) for a, p in zip(actual, predicted)])


def distributed_cumulative_gain(r, k, method):
    r = np.asfarray(r)[:k]
    if r.size:
        if method == 0:
            return r[0] + np.sum(r[1:] / np.log2(np.arange(2, r.size + 1)))
        elif method == 1:
            return np.sum(r / np.log2(np.arange(2, r.size + 2)))
        else:
            raise ValueError('method must be 0 or 1.')

    return 0.


def normalized_distributed_cumulative_gain(r, k, method):
    dcg_max = distributed_cumulative_gain(sorted(r, reverse=True), k, method)
    if not dcg_max:
        return 0

    return distributed_cumulative_gain(r, k, method) / dcg_max


def mean_normalized_distributed_cumulative_gain(r, k, method=0):
    return np.mean([normalized_distributed_cumulative_gain(click, k, method) for click in r])
