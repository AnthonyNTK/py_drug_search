import json
from server import app

app.testing = True


def test_endpoint():
    print("Test suite [Flask endpoint - normal] begins....")

    with app.test_client() as client:
        result = client.get('/')
        assert result.status_code == 200
        assert result.data == b'Hello world'

        print("--> Homepage request test pass.")

        data = {'doc_id': '1'}
        result = client.post('/api/search/id', data=json.dumps(data))
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 200
        assert len(payload['result']) == 1

        print("--> Exact drug search request test pass.")

        result = client.get('/api/search/drugs?query=Lepirudin')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 200
        assert payload['length'] == 1
        assert payload['query'] == 'lepirudin'
        assert len(payload['batch']) == 0
        assert len(payload['doc-id']) == 1

        print("--> Drug name search request test pass.")

        result = client.get('/api/search/toxicity?query=pain+cough+fever')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 200
        assert payload['length'] == 6
        assert payload['query'] == 'pain cough fever'
        assert len(payload['batch']) == 0
        assert len(payload['doc-id']) == 6

        print("--> Drug toxicity search request test pass.")

        result = client.get('/api/search/indication?query=treatment+of+pain')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 200
        assert payload['length'] == 103
        assert payload['query'] == 'treatment of pain'
        assert len(payload['batch']) == 93
        assert len(payload['doc-id']) == 10

        print("--> Drug indication search request test pass.")

        result = client.get('/api/search/description?query=severe+headache')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 200
        assert payload['length'] == 6
        assert payload['query'] == 'severe headache'
        assert len(payload['batch']) == 0
        assert len(payload['doc-id']) == 6

        print("--> Drug description search request test pass.")

        syndrome = "fever arthralgia maculopapular rash"
        data = {"syndrome": syndrome}
        result = client.post('/api/predict/syndrome', data=json.dumps(data))
        payload = json.loads(result.data.decode('utf-8'))
        assert len(payload['disease']) == 4
        assert len(payload['syndrome']) == len(syndrome.split())
        assert payload['syndrome'] == syndrome.split()

        print("--> Model prediction request test pass.")

    print("Test suite [Flask server] ended\n")


def test_exception_request():
    print("Test suite [Flask endpoint - exception] begins....")

    with app.test_client() as client:
        result = client.get('/api/search/drugs?query=sneezing')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 400
        assert payload['error'] == 'No related result found'

        print("--> Drug name exception search request test pass.")

        result = client.get('/api/search/description?query=sneezing')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 300
        assert payload['error'] == 'No related result found'
        assert len(payload['suggestions']) == 1
        assert 'sneeze' in payload['suggestions']

        print("--> Drug description exception search request test pass.")

        result = client.get('/api/search/indication?query=sneezing')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 300
        assert payload['error'] == 'No related result found'
        assert len(payload['suggestions']) == 1
        assert 'sneeze' in payload['suggestions']

        print("--> Drug indication exception search request test pass.")

        result = client.get('/api/search/toxicity?query=run')
        payload = json.loads(result.data.decode('utf-8'))
        assert result.status_code == 300
        assert payload['error'] == 'No related result found'
        assert len(payload['suggestions']) == 3
        assert 'runny' in payload['suggestions']

        print("--> Drug toxicity exception search request test pass.")

        syndrome = "a b c"
        data = {"syndrome": syndrome}
        result = client.post('/api/predict/syndrome', data=json.dumps(data))
        payload = json.loads(result.data.decode('utf-8'))
        assert payload['error'] == 'No prediction can be made'

        print("--> Model prediction exception request test pass.")

    print("Test suite [Flask server] ended\n")
