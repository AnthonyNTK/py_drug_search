import re
import nltk
import verb
import exception
import Singular


def test_symbolic_exception():
    print("Test suite [Symbolic check] begins....")

    word = '<a>Lepuridin\'s-αβ-,[]()*&</a>+=_'
    assert re.sub(exception.symbol_exception, '', word.lower()) == 'lepuridin'

    print("--> special symbol test pass.")

    word = 'a❤b☀c☆d☂e☻f♞g☯i☭j☢k€l→'
    assert re.sub(exception.unicode_exception, '', word) == 'abcdefgijkl'

    print("--> unicode symbol test pass.")

    word = 'tests[1][a][need citation][❤☀☆☂☻♞]'
    assert re.sub(exception.wiki_content_exception, '', word) == 'tests'

    print("--> Wiki content exception test pass.")
    print("Test suite [Symbolic check] ended\n")


def test_pos_checking():
    print("Test suite [Part Of Speech check] begins....")

    valid = False
    words = 'sclerosis possess emmaus endarus exogenous antibioticus meningitis tachyarrhythmias'
    for word in words.split():
        valid = bool(re.match(exception.singular_exception, word))
    assert valid is True

    print("--> special singular form test pass.")

    word = 'effectively'
    word2 = 'favourably'
    assert bool(re.match(exception.pos_exception[0], word)) is True
    assert bool(re.match(exception.pos_exception[1], word2)) is True

    print("--> special POS (ADJ, ADV) test pass.")

    checker = Singular.Singular()
    words = ['uses']
    pos_tags = nltk.pos_tag(words)
    for word, tag in pos_tags:
        singular_form = checker.singular(word)
        assert singular_form == 'use'

    print("--> Verb singularization test pass.")

    checker = verb.Tense()
    checker.initialize()
    word = 'played'
    potential_verb = checker.verb_infinitive(word)
    assert word != potential_verb
    assert potential_verb == 'play'
    assert checker.verb_is_past(word) is True

    word = 'playing'
    assert checker.verb_is_present_participle(word) is True

    word = 'done'
    assert checker.verb_is_past_participle(word) is True

    word = 'was'
    assert checker.verb_is_tense(word, "1st singular past") is True

    word = 'cough'
    assert checker.verb_is_tense(word, 'inf') is True

    print("--> Verb tense test pass.")
    print("Test suite [Part Of Speech check] ended\n")
