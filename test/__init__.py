""" DESCRIPTION:
        @Author:     NG Tsz kin
        @Version:    0.0.2
        @Purpose:    Class holder to run all unit tests and statistical test/measurement
"""
import time
import traceback
from test import engine
from test import request
from test import validation
from test import ranking


class Online:
    @staticmethod
    def run_all_test():
        pass_count = 0
        fail_count = 0
        try:
            engine.test_engine_instance()

            pass_count += 1
        except AssertionError:
            traceback.print_exc()

            fail_count += 1

        try:
            engine.test_engine_function()

            pass_count += 1
        except AssertionError:
            traceback.print_exc()

            fail_count += 1

        try:
            request.test_endpoint()

            pass_count += 1
        except AssertionError:
            traceback.print_exc()

            fail_count += 1

        try:
            request.test_exception_request()

            pass_count += 1
        except AssertionError:
            traceback.print_exc()

            fail_count += 1

        return pass_count, fail_count


class Offline:
    @staticmethod
    def run_all_test():
        pass_count = 0
        fail_count = 0

        try:
            validation.test_symbolic_exception()

            pass_count += 1
        except AssertionError:
            traceback.print_exc()

            fail_count += 1

        try:
            validation.test_pos_checking()

            pass_count += 1
        except AssertionError:
            traceback.print_exc()

            fail_count += 1

        return pass_count, fail_count


class Ranking:
    def __init__(self):
        # 18 in total 6 for each aspect in following order (indication --> toxicity --> description)
        self.actual = [
            [4611, 4895, 1175, 4713, 4700, 4705, 4744, 4745, 4746, 4844],     # treatment of pain
            [9035, 348, 327, 1052, 1111, 1554, 619, 4540, 331, 392],          # fever
            [4892, 5365, 5663, 1, 9711, 5636, 9784, 232, 266, 4],             # thrombocytopenia
            [5844, 5867, 852, 8166, 5804, 976, 769, 8168, 9853, 1344],        # pain and inflammation
            [5307, 4780, 5035, 9711, 4631, 4753, 5676, 5570, 5480, 4616],     # liver disease
            [4887, 61, 8169, 762, 7802, 4, 5850, 5491, 1061, 1093],           # acute coronary syndrome
            [933, 1240, 5578, 989, 9716, 366, 8026, 1085, 351, 5476],         # metastatic breast therapy and treatment
            [456, 5039, 852, 1569, 481, 5064, 5804, 976, 769, 9853],          # rheumatoid arthritis pain
            [5629, 5018, 5511, 5622, 5537, 5508, 5728, 1212, 2846, 5472],     # ovarian cancer or lung cancer
            [4763, 4767, 4769, 4827, 5345, 5359, 5738, 4944, 5366, 5559],     # macular degeneration
            [854, 526, 1015, 578, 716, 777, 1184, 620, 769, 1100],            # severe headache
            [412, 720, 459, 1358, 587, 1029, 8112, 991, 924, 5861],           # dizziness and fever
            [986, 1089, 414, 7860, 526, 865, 695, 991, 120, 7877],            # cough and nausea
            [799, 1268, 1323, 921, 704, 462, 4757, 8120, 493, 1087],          # insomnia and dry mouth and tremor
            [4589, 656, 1288, 1293, 1224, 4616, 756, 8054, 996, 816],         # abdominal pain and vomiting and diarrhea
            [535, 229, 515, 605, 5578, 8190, 7873, 5720, 8068, 8054],         # fatigue and rash
            [215, 1130, 8070, 10949, 9051, 9255, 9145, 13, 8350, 10645],      # oral and throat
            [1021, 666, 418, 1216, 1161, 5017, 4661, 375, 917, 914],          # hypotension and tachycardia
            [217, 1329, 409, 821, 425, 550, 368, 1049, 834, 667],             # coma seizure
            [302, 716, 220, 301, 8137, 1344, 168, 8218, 8340, 373],           # electrolyte disturbance
            [4620, 4804, 535, 5636, 4658, 9414, 366, 5193, 5454, 848],        # immunosuppressive drug
            [10210, 10561, 10503, 10129, 9824, 10112, 5836, 9380, 8170, 752], # congenital treatment
            [1857, 3397, 3909, 2518, 1368, 960, 4602, 5533, 5818, 4830],      # phosphodiesterase inhibitor
            [5676, 1027, 10191, 743, 4731, 9820, 11087, 10306, 11133, 11100], # intravenous therapy
            [9938, 674, 10836, 1072, 526, 859, 7988, 956, 547, 264],          # high blood pressure
            [10320, 10359, 5259, 5495, 10262, 10148, 495, 1368, 526, 1123],   # congestive heart failure
            [4667, 10791, 5082, 10459, 27, 9545, 104, 5605, 7, 25],           # autoimmune disorder
            [4575, 5750, 7971, 8249, 5328, 250, 5541, 8343, 9560, 5771],      # blood injection
            [5836, 8170, 8001, 9357, 91, 9223, 10591, 9332, 8257, 11113],     # congenital deficiency
            [4289, 769, 996, 1057, 574, 7953, 778, 852, 698, 5241]            # anti-inflammatory agent with analgesic
        ]
        self.predicted = [
            [4611, 4895, 1175, 4700, 4705, 4744, 4745, 4746, 4844, 4860],
            [327, 9035, 4540, 348, 1052, 1111, 1554, 619, 331, 392],
            [1, 9711, 4892, 9784, 4, 4731, 5365, 5663, 5636, 910],
            [5844, 5867, 852, 5804, 976, 769, 9853, 1344, 5793, 8161],
            [5307, 4780, 5035, 4631, 4753, 5676, 5570, 5480, 7, 844],
            [4887, 61, 8169, 762, 7802, 4, 5491, 1061, 624, 5850],
            [933, 1240, 5578, 989, 9716, 366, 8026, 1085, 351, 1061],
            [456, 5039, 852, 1569, 481, 5064, 5804, 976, 769, 9853],
            [5629, 5018, 5511, 5622, 351, 5508, 429, 1212, 2846, 5472],
            [4763, 4767, 4769, 684, 5345, 5359, 5738, 4944, 5366, 5559],
            [854, 526, 1015, 578, 716, 777, 1184, 620, 769, 1100],
            [412, 720, 459, 1358, 587, 1029, 8112, 991, 924, 5861],
            [986, 1089, 414, 7860, 526, 865, 695, 991, 120, 7877],
            [799, 1268, 1323, 921, 704, 462, 4757, 8120, 493, 330],
            [4589, 656, 1288, 1293, 4616, 756, 8054, 996, 816, 676],
            [605, 535, 229, 515, 5578, 8190, 7873, 5720, 8068, 8054],
            [215, 1130, 8070, 10949, 9051, 9255, 10949, 13, 88, 11000],
            [1021, 666, 418, 1216, 1161, 1146, 4661, 375, 917, 914],
            [217, 1329, 409, 821, 425, 550, 368, 1049, 834, 667],
            [302, 716, 220, 301, 8137, 1344, 168, 8218, 8340, 373],
            [4620, 4804, 5636, 4658, 366, 5193, 5454, 848, 8819, 8810],
            [10210, 10561, 10503, 10129, 9824, 10112, 5836, 9380, 8170, 8001],
            [1857, 3397, 3909, 2518, 1368, 960, 4602, 5533, 5818, 5338],
            [5676, 1027, 10191, 4731, 9820, 8343, 11032, 9362, 9331, 5700],
            [9938, 674, 10836, 1072, 526, 859, 7988, 956, 547, 264],
            [10320, 10359, 5259, 5495, 10262, 10148, 495, 1368, 526, 1123],
            [4667, 10006, 5082, 10459, 27, 9545, 104, 5605, 7, 25],
            [1845, 5750, 8296, 8249, 5328, 250, 5541, 8343, 9560, 5771],
            [5836, 8170, 8094, 10857, 91, 9223, 10591, 9332, 8257, 11113],
            [9227, 769, 996, 1057, 574, 7953, 778, 852, 698, 9483]
        ]

        self.actual_click = [
            [10, 9, 8, 7, 6, 3, 5, 4, 0, 0],
            [8, 10, 1, 9, 2, 7, 6, 5, 4, 3],
            [7, 6, 10, 4, 1, 0, 9, 7, 5, 0],
            [6, 9, 8, 10, 5, 4, 3, 0, 0, 7],
            [8, 9, 10, 6, 5, 4, 7, 3, 0, 0],
            [0, 10, 8, 7, 6, 5, 3, 2, 9, 4],
            [0, 9, 8, 7, 6, 5, 4, 3, 2, 10],
            [10, 7, 8, 9, 6, 1, 4, 3, 2, 5],
            [10, 0, 8, 7, 9, 5, 0, 3, 2, 1],
            [5, 9, 8, 0, 6, 10, 5, 4, 3, 2],    # 10
            [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
            [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
            [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
            [10, 9, 8, 7, 6, 5, 4, 3, 2, 0],
            [1, 9, 8, 7, 5, 4, 3, 2, 10, 0],
            [0, 9, 8, 7, 6, 5, 4, 3, 2, 1],
            [10, 9, 8, 7, 5, 4, 0, 2, 0, 0],
            [10, 9, 8, 7, 5, 0, 4, 3, 2, 1],
            [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
            [8, 9, 10, 7, 6, 5, 4, 3, 2, 1],    # 20
            [7, 9, 10, 6, 0, 3, 2, 1, 0, 0],
            [5, 9, 8, 7, 6, 10, 4, 3, 2, 0],
            [3, 9, 8, 7, 6, 5, 4, 10, 2, 0],
            [1, 4, 8, 7, 6, 5, 9, 3, 2, 10],
            [6, 9, 8, 7, 10, 5, 4, 3, 2, 1],
            [0, 9, 7, 8, 6, 10, 4, 3, 2, 1],
            [10, 0, 4, 7, 6, 5, 8, 3, 2, 1],
            [0, 9, 0, 7, 6, 5, 4, 3, 2, 1],
            [10, 2, 8, 0, 6, 5, 4, 3, 9, 1],
            [0, 9, 8, 7, 6, 5, 4, 3, 2, 0],    # 30
        ]

        self.relevance = [
            [1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 0, 1, 1, 1, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [1, 1, 1, 1, 0, 1, 1, 1, 0, 0],
            [1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 0, 1, 0, 1, 1, 1],
            [1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 0, 1, 1, 1, 1, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 0, 1, 0, 0, 1, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 0, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 0]
        ]

    def print_matching(self):
        for i, (predict, actual) in enumerate(zip(self.predicted, self.actual)):
            print(i + 1,  predict)
            print(i + 1, actual)
            print('\n')

    def run_statistical_test(self):
        indication_map = ranking.mean_average_precision(self.actual[0:10], self.predicted[0:10])
        toxicity_map = ranking.mean_average_precision(self.actual[10:20], self.predicted[10:20])
        description_map = ranking.mean_average_precision(self.actual[20:30], self.predicted[20:30])
        overall_map = ranking.mean_average_precision(self.actual, self.predicted)
        indication_ndcg = ranking.mean_normalized_distributed_cumulative_gain(self.actual_click[0:10], 10, method=1)
        toxicity_ndcg = ranking.mean_normalized_distributed_cumulative_gain(self.actual_click[10:20], 10, method=1)
        description_ndcg = ranking.mean_normalized_distributed_cumulative_gain(self.actual_click[20:30], 10, method=1)
        overall_ndcg = ranking.mean_normalized_distributed_cumulative_gain(self.actual_click, 10, method=1)
        indication_mrp = ranking.mean_x_precision(self.relevance[0:10])
        toxicity_mrp = ranking.mean_x_precision(self.relevance[10:20])
        description_mrp = ranking.mean_x_precision(self.relevance[20:30])
        overall_mrp = ranking.mean_x_precision(self.relevance)

        indication_mkp_3 = ranking.mean_x_precision(self.relevance[0:10], 3)
        indication_mkp_4 = ranking.mean_x_precision(self.relevance[0:10], 4)
        indication_mkp_5 = ranking.mean_x_precision(self.relevance[0:10], 5)
        indication_mkp_6 = ranking.mean_x_precision(self.relevance[0:10], 6)
        indication_mkp_7 = ranking.mean_x_precision(self.relevance[0:10], 7)
        indication_mkp_8 = ranking.mean_x_precision(self.relevance[0:10], 8)
        indication_mkp_9 = ranking.mean_x_precision(self.relevance[0:10], 9)
        indication_mkp_10 = ranking.mean_x_precision(self.relevance[0:10], 10)

        toxicity_mkp_3 = ranking.mean_x_precision(self.relevance[10:20], 3)
        toxicity_mkp_4 = ranking.mean_x_precision(self.relevance[10:20], 4)
        toxicity_mkp_5 = ranking.mean_x_precision(self.relevance[10:20], 5)
        toxicity_mkp_6 = ranking.mean_x_precision(self.relevance[10:20], 6)
        toxicity_mkp_7 = ranking.mean_x_precision(self.relevance[10:20], 7)
        toxicity_mkp_8 = ranking.mean_x_precision(self.relevance[10:20], 8)
        toxicity_mkp_9 = ranking.mean_x_precision(self.relevance[10:20], 9)
        toxicity_mkp_10 = ranking.mean_x_precision(self.relevance[10:20], 10)

        description_mkp_3 = ranking.mean_x_precision(self.relevance[20:30], 3)
        description_mkp_4 = ranking.mean_x_precision(self.relevance[20:30], 4)
        description_mkp_5 = ranking.mean_x_precision(self.relevance[20:30], 5)
        description_mkp_6 = ranking.mean_x_precision(self.relevance[20:30], 6)
        description_mkp_7 = ranking.mean_x_precision(self.relevance[20:30], 7)
        description_mkp_8 = ranking.mean_x_precision(self.relevance[20:30], 8)
        description_mkp_9 = ranking.mean_x_precision(self.relevance[20:30], 9)
        description_mkp_10 = ranking.mean_x_precision(self.relevance[20:30], 10)

        mkp_3 = ranking.mean_x_precision(self.relevance, 3)
        mkp_4 = ranking.mean_x_precision(self.relevance, 4)
        mkp_5 = ranking.mean_x_precision(self.relevance, 5)
        mkp_6 = ranking.mean_x_precision(self.relevance, 6)
        mkp_7 = ranking.mean_x_precision(self.relevance, 7)
        mkp_8 = ranking.mean_x_precision(self.relevance, 8)
        mkp_9 = ranking.mean_x_precision(self.relevance, 9)
        mkp_10 = ranking.mean_x_precision(self.relevance, 10)

        print('--> Mean average precision (Indication):                             ' + str(indication_map))
        print('--> Mean average precision (Toxicity):                               ' + str(toxicity_map))
        print('--> Mean average precision (Description):                            ' + str(description_map))
        print('--> Mean average precision (Overall):                                ' + str(overall_map))
        print('--> Mean Normalized distributed cumulative gain (Indication):        ' + str(indication_ndcg))
        print('--> Mean Normalized distributed cumulative gain (Toxicity):          ' + str(toxicity_ndcg))
        print('--> Mean Normalized distributed cumulative gain (Description):       ' + str(description_ndcg))
        print('--> Mean Normalized distributed cumulative gain (Overall):           ' + str(overall_ndcg))
        print('--> Mean R-Precision (Indication):                                   ' + str(indication_mrp))
        print('--> Mean R-Precision (Toxicity):                                     ' + str(toxicity_mrp))
        print('--> Mean R-Precision (Description):                                  ' + str(description_mrp))
        print('--> Mean R-Precision (Overall):                                      ' + str(overall_mrp))
        print('--> Mean K-Precision (Indication, k = 3):                            ' + str(indication_mkp_3))
        print('--> Mean K-Precision (Indication, k = 4):                            ' + str(indication_mkp_4))
        print('--> Mean K-Precision (Indication, k = 5):                            ' + str(indication_mkp_5))
        print('--> Mean K-Precision (Indication, k = 6):                            ' + str(indication_mkp_6))
        print('--> Mean K-Precision (Indication, k = 7):                            ' + str(indication_mkp_7))
        print('--> Mean K-Precision (Indication, k = 8):                            ' + str(indication_mkp_8))
        print('--> Mean K-Precision (Indication, k = 9):                            ' + str(indication_mkp_9))
        print('--> Mean K-Precision (Indication, k = 10):                           ' + str(indication_mkp_10))
        print('--> Mean K-Precision (Toxicity, k = 3):                              ' + str(toxicity_mkp_3))
        print('--> Mean K-Precision (Toxicity, k = 4):                              ' + str(toxicity_mkp_4))
        print('--> Mean K-Precision (Toxicity, k = 5):                              ' + str(toxicity_mkp_5))
        print('--> Mean K-Precision (Toxicity, k = 6):                              ' + str(toxicity_mkp_6))
        print('--> Mean K-Precision (Toxicity, k = 7):                              ' + str(toxicity_mkp_7))
        print('--> Mean K-Precision (Toxicity, k = 8):                              ' + str(toxicity_mkp_8))
        print('--> Mean K-Precision (Toxicity, k = 9):                              ' + str(toxicity_mkp_9))
        print('--> Mean K-Precision (Toxicity, k = 10):                             ' + str(toxicity_mkp_10))
        print('--> Mean K-Precision (Description, k = 3):                           ' + str(description_mkp_3))
        print('--> Mean K-Precision (Description, k = 4):                           ' + str(description_mkp_4))
        print('--> Mean K-Precision (Description, k = 5):                           ' + str(description_mkp_5))
        print('--> Mean K-Precision (Description, k = 6):                           ' + str(description_mkp_6))
        print('--> Mean K-Precision (Description, k = 7):                           ' + str(description_mkp_7))
        print('--> Mean K-Precision (Description, k = 8):                           ' + str(description_mkp_8))
        print('--> Mean K-Precision (Description, k = 9):                           ' + str(description_mkp_9))
        print('--> Mean K-Precision (Description, k = 10):                          ' + str(description_mkp_10))
        print('--> Mean K-Precision (Overall, k = 3):                               ' + str(mkp_3))
        print('--> Mean K-Precision (Overall, k = 4):                               ' + str(mkp_4))
        print('--> Mean K-Precision (Overall, k = 5):                               ' + str(mkp_5))
        print('--> Mean K-Precision (Overall, k = 6):                               ' + str(mkp_6))
        print('--> Mean K-Precision (Overall, k = 7):                               ' + str(mkp_7))
        print('--> Mean K-Precision (Overall, k = 8):                               ' + str(mkp_8))
        print('--> Mean K-Precision (Overall, k = 9):                               ' + str(mkp_9))
        print('--> Mean K-Precision (Overall, k = 10):                              ' + str(mkp_10))


def main():
    start_time = time.time()

    overall_success = 0
    overall_fail = 0

    success, fail = Online.run_all_test()
    overall_success += success
    overall_fail += fail

    success, fail = Offline.run_all_test()
    overall_success += success
    overall_fail += fail

    end_time = time.time()

    print("Ran " + str(overall_success + overall_fail) +
          " tests: [" + str(overall_success) + " passed, " + str(overall_fail) + " failed].")
    print("[All Test Suite finished in " + str(end_time - start_time) + "s]\n")
    print("Starting to run ranking measurement.....")

    ranking_module = Ranking()
    ranking_module.run_statistical_test()

    print("Measurement ended.")


if __name__ == "__main__":
    main()
