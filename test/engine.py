import DrugSearch


def test_engine_instance():
    print("Test suite [engine instance] begins....")

    engine = DrugSearch.SearchEngine()
    assert engine.valid_count == 0
    assert engine.previous_search_endpoint == ''
    assert len(engine.drug) == 0
    assert len(engine.index) == 0
    assert len(engine.length_vectors) == 0
    assert len(engine.cosine_similarity) == 0

    print("--> Empty engine test pass.")

    instance = engine.initialize(
        mode='drugs',
        drug_file=DrugSearch.metadata.drug_index_file
    )
    assert instance.previous_search_endpoint == 'drugs'
    assert len(instance.drug) != 0

    print("--> Engine instance change test pass. (To drug match)")

    instance = engine.initialize(
        mode='indication',
        index_file=DrugSearch.metadata.indication_index_file,
        vector_file=DrugSearch.metadata.indication_length_file
    )
    assert instance.previous_search_endpoint == 'indication'
    assert len(instance.drug) != 0
    assert len(instance.index) != 0
    assert len(instance.length_vectors) != 0
    assert len(instance.cosine_similarity) != 0

    print("--> Engine instance change test pass. (To indication search)")
    print("Test suite [engine instance] ended\n")


def test_engine_function():
    print("Test suite [engine function] begins....")

    engine = DrugSearch.SearchEngine()
    instance = engine.initialize(
        mode='drugs',
        drug_file=DrugSearch.metadata.drug_index_file
    )
    result = instance.find_exact_drug('lepirudin')
    assert len(result) == 1
    assert result[0]['doc_id'] == '1'

    print("--> Exact drug match function test pass.")

    instance = engine.initialize(
        mode='indication',
        index_file=DrugSearch.metadata.indication_index_file,
        vector_file=DrugSearch.metadata.indication_length_file
    )
    result = instance.find_relevance('sneezing')
    assert len(result) == 0

    print("--> Drug indication search function test pass. (Exception test)")

    result = instance.find_relevance('treatment of pain')
    assert len(result) == 103
    assert instance.valid_count == 2

    print("--> Drug indication search function test pass. (Normal test)")

    instance = engine.initialize(
        mode='toxicity',
        index_file=DrugSearch.metadata.toxicity_index_file,
        vector_file=DrugSearch.metadata.toxicity_length_file
    )
    result = instance.find_relevance('pain cough fever')
    assert len(result) == 6
    assert instance.valid_count == 3

    print("--> Drug toxicity search function test pass. (Normal test)")

    instance = engine.initialize(
        mode='description',
        index_file=DrugSearch.metadata.description_index_file,
        vector_file=DrugSearch.metadata.description_length_file
    )
    result = instance.find_relevance('severe headache')
    assert len(result) == 6
    assert instance.valid_count == 2

    print("--> Drug description search function test pass. (Normal test)")
    print("Test suite [engine function] ended\n")
