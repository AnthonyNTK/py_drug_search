import os

verb_tenses_keys = {
    "infinitive"           : 0,
    "1st singular present" : 1,
    "2nd singular present" : 2,
    "3rd singular present" : 3,
    "present plural"       : 4,
    "present participle"   : 5,
    "1st singular past"    : 6,
    "2nd singular past"    : 7,
    "3rd singular past"    : 8,
    "past plural"          : 9,
    "past"                 : 10,
    "past participle"      : 11
}

verb_tenses_aliases = {
    "inf"     : "infinitive",
    "1sgpres" : "1st singular present",
    "2sgpres" : "2nd singular present",
    "3sgpres" : "3rd singular present",
    "pl"      : "present plural",
    "prog"    : "present participle",
    "1sgpast" : "1st singular past",
    "2sgpast" : "2nd singular past",
    "3sgpast" : "3rd singular past",
    "pastpl"  : "past plural",
    "pt"      : "past",
    "ppart"   : "past participle"
}


class Tense:
    def __init__(self):
        self.verb_tenses = {}
        self.verb_lemmas = {}

    def initialize(self):
        path = os.path.join(os.path.dirname(__file__), "verb.txt")
        data = open(path).readlines()
        for i in range(len(data)):
            a = data[i].strip().split(",")
            self.verb_tenses[a[0]] = a

        # Each verb can be lemmatised:
        # inflected morphs of the verb point
        # to its infinitive in this dictionary.

        for infinitive in self.verb_tenses:
            for tense in self.verb_tenses[infinitive]:
                if tense != "":
                    self.verb_lemmas[tense] = infinitive

    def verb_infinitive(self, v):
        try:
            return self.verb_lemmas[v]
        except KeyError:
            return ""

    def verb_conjugate(self, v, tense="infinitive", negate=False):
        v = self.verb_infinitive(v)
        i = verb_tenses_keys[tense]
        if negate is True:
            i += len(verb_tenses_keys)

        return self.verb_tenses[v][i]

    def verb_present(self, v, person="", negate=False):
        person = str(person).replace("pl", "*").strip("stndrgural")
        v_hash = {
            "1": "1st singular present",
            "2": "2nd singular present",
            "3": "3rd singular present",
            "*": "present plural",
        }
        if person in v_hash and self.verb_conjugate(v, v_hash[person], negate) != "":
            return self.verb_conjugate(v, v_hash[person], negate)

        return self.verb_conjugate(v, "infinitive", negate)

    def verb_present_participle(self, v):
        return self.verb_conjugate(v, "present participle")

    def verb_past(self, v, person="", negate=False):
        person = str(person).replace("pl", "*").strip("stndrgural")
        v_hash = {
            "1": "1st singular past",
            "2": "2nd singular past",
            "3": "3rd singular past",
            "*": "past plural",
        }
        if person in v_hash and self.verb_conjugate(v, v_hash[person], negate) != "":
            return self.verb_conjugate(v, v_hash[person], negate)

        return self.verb_conjugate(v, "past", negate)

    def verb_past_participle(self, v):
        return self.verb_conjugate(v, "past participle")

    def verb_tense(self, v):
        infinitive = self.verb_infinitive(v)
        try:
            a = self.verb_tenses[infinitive]
        except KeyError:
            return ""

        for tense in verb_tenses_keys:
            if a[verb_tenses_keys[tense]] == v:
                return tense
            if a[verb_tenses_keys[tense] + len(verb_tenses_keys)] == v:
                return tense

    def verb_is_tense(self, v, tense):
        if tense in verb_tenses_aliases:
            tense = verb_tenses_aliases[tense]
        if self.verb_tense(v) == tense:
            return True
        else:
            return False

    def verb_is_present(self, v, person="", negated=False):
        person = str(person).replace("*", "plural")
        tense = self.verb_tense(v)
        if tense is not None:
            if "present" in tense and person in tense:
                if negated is False:
                    return True
                elif "n't" in v or " not" in v:
                    return True

        return False

    def verb_is_present_participle(self, v):
        tense = self.verb_tense(v)
        if tense == "present participle":
            return True
        else:
            return False

    def verb_is_past(self, v, person="", negated=False):
        person = str(person).replace("*", "plural")
        tense = self.verb_tense(v)
        if tense is not None:
            if "past" in tense and person in tense:
                if negated is False:
                    return True
                elif "n't" in v or " not" in v:
                    return True

        return False

    def verb_is_past_participle(self, v):
        tense = self.verb_tense(v)
        if tense == "past participle":
            return True
        else:
            return False

    @staticmethod
    def verb_all_tenses():
        return verb_tenses_keys.keys()
