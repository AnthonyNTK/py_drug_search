typo_word = ['alum', 'reversal', 'psoriatic', 'construction', 'dexpanthanol', 'dihydrofolic', 'methylation',
             'hydroxylysine', 'calcidiol', 'phosphatidyl', 'hydroxycobalamin', 'bacteriostatic', 'schizophrenic',
             'thrombocytosis', 'tcas', 'adrenalectomy', 'cannabinoids', 'digitalization', 'cisplatinum',
             'resemblance', 'enalaprilat', 'skipped', 'meglitnides', 'ha-receptors', 'colistinus', 'colistins',
             'temodar', 'pdes', 'quinaprilat', 's-isomers', 'flavanone', 'lignans', 'epimers', 'catechins',
             'zuclopentixol', 'zuclopenthixolum', 'dheas', 'phenylpropylamines', 'phenylhydrazines', 'psoralens',
             'integrins', 'nachrs', 'cadherins', 'teicoplanins', 'phenylpiperazines', 'pyrrolotriazines',
             'oxepanes', 'phenylpyrazoles', 'isoquinolines', 'radiolabelled', 'stitipentol', 'umps', 'bdzs', 'tau',
             'ombutasvir', 'titaniumiv', 'refining', 'depsidones', 'depsides', 'repellent', 'malacidins', 'reponse',
             'found', 'following', 'collecting', 'decreased', 'used', 't-cells', 'viruses', 'nocturnal',
             'streptoc', 'personality', 'scoring', 'addict', 'aged', 'advanced', 'injected', 'suicidal',
             'alum', 'phenylketonurics', 'hydroxycobalamin', 'breath', 'impair', 'gramskg', 'gramkg', 'standardized',
             'haematomas', 'hydrolases', 'phenylpropylamines', 'dequalinuim', 'psoralens', 'integrins', 'cadherins',
             'hydroaldosteronism', 'd-hormones', 'teicoplanins', 'phenylpiperazines', 'pyrrolotriazines', 'oxepanes',
             'phenylpyrazoles', 'radiolabelled', 'empaglifozin', 'amorolfin', 'grapefruit', 'nicoboxilkg', 'achived',
             'inverted', 'inoculated', 'thalliumi', 'rendered', 'oxidized', 'oxidizing', 'owing', 'recieved',
             'emmitted', 'complexed', 'supressing', 'suppressed', 'immunosupressant', 'investigated',
             'hyperprolalctinaemium', 'studied', 'gestoden', 'proposed', 'weigh', 'owned', 'planned', 'simprevir',
             'determining', 'bruising', 'condensed', 'thickening', 'titaniumiv', 'labelling', 'labeling',
             'radio-labelled', 'radiolabeling', 'radiolabeled', 'designated', 'normalized', 'leading', 'slowing',
             'feeding', 'treatement', 'penumonia']

correct_word = ['aluminum', 'reverse', 'psoriasis', 'construct', 'dexpanthenol', 'dihydrofolate', 'methyl-',
                'hydroxylase', 'calcium', 'phospholipid', 'hydroxocobalamin', 'bactericidal', 'schizophrenia',
                'thrombocythemia', 'tca', 'adrenal', 'cannabinoid', 'digitoxin', 'cisplatin', 'resemble', 'enalapril',
                'skip', 'meglitinides', 'ha-receptor', 'colistin', 'colistin', 'temodal', 'pde', 'quinapril',
                's-isomer', 'flavonoid', 'lignan', 'epimer', 'catechin', 'zuclopenthixol', 'zuclopenthixol', 'dhea',
                'phenylpropylamine', 'phenylhydrazine', 'psoralen', 'integrin', 'nachr', 'cadherin', 'teicoplanin',
                'phenylpiperazine', 'pyrrolotriazine', 'oxepane', 'phenylpyrazole', 'isoquinoline', 'radiolabel',
                'stiripentol', 'ump', 'bdz', 'taurx', 'ombitasvir', 'titanium', 'refine', 'depsidone', 'depside',
                'repel', 'malacidin', 'response', 'find', 'follow', 'collect', 'decrease', 'use', 't-cell', 'virus',
                'nocturia', 'streptococci', 'person', 'score', 'addiction', 'age', 'advance', 'inject', 'suicide',
                'aluminum', 'phenylketonuria', 'hydroxocobalamin', 'breathing', 'impaired', 'gram', 'gram',
                'standardize', 'haematoma', 'hydrolase', 'phenylpropylamine', 'dequalinium', 'psoralen', 'integrin',
                'cadherin', 'aldosteronism', 'd-hormone', 'teicoplanin', 'phenylpiperazine', 'pyrrolotriazine',
                'oxepane', 'phenylpyrazole', 'radiolabel', 'empagliflozin', 'amorolfine', 'grape', 'nicoboxil',
                'achieve', 'invert', 'inoculate', 'thallium', 'render', 'oxidize', 'oxidize', 'owe', 'receive', 'emit',
                'complex', 'suppress', 'suppress', 'immunosuppressant', 'investigate', 'hyperprolactinaemium', 'study',
                'gestodene', 'propose', 'weight', 'own', 'plan', 'simeprevir', 'determine', 'bruise', 'condense',
                'thicken', 'titanium', 'label', 'label', 'radiolabel', 'radiolabel', 'radiolabel', 'designate',
                'normalize', 'lead', 'slow', 'feed', 'treatment', 'pneumonia']

symbol_exception = "'s|<.>|</.>|<em>|</em>|<br>|<br />|<ul>|<li>|</ul>|</li>|--|α-|β-|-β|-α|[\\\".*\[\](),;:&_+=\'®<>]"
singular_exception = '.*sis$|.*ss$|.*aus$|.*rus$|.*ous$|.*us$|.*is$|.*as$'
pos_exception = ['.*ly$', '.*bly$']
unicode_exception = '[^\x00-\x7F]+'

wiki_content_exception = '\[(.*?)\]'
wiki_exception = ['Other reasons this message may be displayed', 'may refer to', 'Return to Main Page',
                  'can refer to', 'usually refers to', 'may also refer to']

ontology_syndrome = '(?=has symptom)(.*)(?=,)'
ontology_exception = 'has symptom |and '

