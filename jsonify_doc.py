import os
import re
import json
import time
import MetaData

metadata = MetaData.MetaData()


def read_file():
    doc_id_list = [int(re.sub('.txt', '', docID_string)) for docID_string in os.listdir(metadata.process_metadata[0])]
    doc_id_list.sort()

    output_file = open(metadata.process_metadata[1], 'w', encoding='utf8')

    for docID in doc_id_list:
        file_path = os.path.join(metadata.process_metadata[0], str(docID) + '.txt')
        with open(file_path, encoding='utf8') as input_file:
            contents = input_file.read()
            record = [docID, contents]
            output_file.write(json.dumps(record) + '\n')

        print(str(docID))

    output_file.close()


def main():
    t0 = time.time()

    metadata.set_fda_indication_metadata()
    read_file()

    t1 = time.time()
    print('Processing time: ' + str(t1 - t0))


if __name__ == '__main__':
    main()
