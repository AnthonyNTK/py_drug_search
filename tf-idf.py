import MetaData
import MapReduce
from math import sqrt

mr = MapReduce.MapReduce()
metadata = MetaData.MetaData()


def mapper(record):
    docs = record[2]
    for doc in docs:
        mr.emit_intermediate(doc[0], doc[1])


def reducer(key, list_of_values):
    score = 0
    for value in list_of_values:        # tf-idf scores of each term
        square = value**2
        score = score + square

    # length = sqrt(score)
    mr.emit((key, score))


def main():
    metadata.set_disease_metadata()

    with open(metadata.process_metadata[2], encoding='utf8') as input_data:
        output = open(metadata.process_metadata[3], 'w', encoding='utf8')
        mr.execute(input_data, mapper, reducer, output)
        output.close()


if __name__ == '__main__': 
    main()
