@echo off
set runType=%1

IF runType == "dev" (
	GOTO devSetup
)

IF runType == "debug" (
	GOTO debugSetup
)

IF runType == "prod" (
	GOTO prodSetup
)
exit /b

:devSetup
IF NOT EXIST %cd%\description\ (
	mkdir description
	echo Folder 'description' created!
) ELSE (
	echo Folder 'description' already exist!
)
IF NOT EXIST %cd%\indication\ (
	mkdir indication
	echo Folder 'indication' created!
) ELSE (
	echo Folder 'indication' already exist!
)
IF NOT EXIST %cd%\toxicity\ (
	mkdir toxicity
	echo Folder 'toxicity' created!
) ELSE (
	echo Folder 'toxicity' already exist!
)
IF NOT EXIST %cd%\statistic\ (
	mkdir statistic
	echo Folder 'statistic' created!
) ELSE (
	echo Folder 'statistic' already exist!
)
IF NOT EXIST %cd%\important\ (
	mkdir important
	echo Folder 'important' created!
) ELSE (
	echo Folder 'important' already exist!
)
IF NOT EXIST %cd%\external\ (
	mkdir external
	echo Folder 'external' created!
) ELSE (
	echo Folder 'external' already exist!
)
IF NOT EXIST %cd%\symptom\ (
	mkdir symptom
	echo Folder 'symptom' created!
) ELSE (
	echo Folder 'symptom' already exist!
)
IF NOT EXIST %cd%\result\ (
	mkdir result
	echo Folder 'result' created!
) ELSE (
	echo Folder 'result' already exist!
)
exit /b

:debugSetup
IF NOT EXIST %cd%\description\ (
	IF NOT EXIST %cd%\indication\ (
		IF NOT EXIST %cd%\toxicity\ (
			setlocal
			cd /d %~dp0
			Call :UnZipFile "%cd%" "%cd%\backup.zip"
		) ELSE (
			echo Folders already exists! 
		)
	) ELSE (
		echo Folders already exists! 
	)
) ELSE (
	echo Folders already exists! 
)
exit /b

:prodSetup
IF NOT EXIST %cd%\statistic\ (
	IF NOT EXIST %cd%\result\ (
		setlocal
		cd /d %~dp0
		Call :UnZipFile "%cd%" "%cd%\convert.zip"
	) ELSE (
		echo Result Folders already exists!
	)

	IF NOT EXIST %cd%\disease\mapping (
		setlocal
		cd /d %~dp0
		Call :UnZipFile "%cd%\disease" "%cd%\disease\mapping.zip"
	) ELSE (
		echo Mapping Folders already exists!
	)
) ELSE (
	echo Folders already exists! You should be ready yo go
)
exit /b

:UnZipFile <ExtractTo> <newzipfile>
set vbs="%temp%\_.vbs"
if exist %vbs% del /f /q %vbs%
>%vbs%  echo Set fso = CreateObject("Scripting.FileSystemObject")
>>%vbs% echo If NOT fso.FolderExists(%1) Then
>>%vbs% echo fso.CreateFolder(%1)
>>%vbs% echo End If
>>%vbs% echo set objShell = CreateObject("Shell.Application")
>>%vbs% echo set FilesInZip=objShell.NameSpace(%2).items
>>%vbs% echo objShell.NameSpace(%1).CopyHere(FilesInZip)
>>%vbs% echo Set fso = Nothing
>>%vbs% echo Set objShell = Nothing
cscript //nologo %vbs%
if exist %vbs% del /f /q %vbs%