import re
import time
import json
import math
import requests
import threading
import collections
from bs4 import BeautifulSoup

THREAD_LIMIT = 100


class Crawler:
    def __init__(self, damping=0.85, loop=10):
        self.loop = loop
        self.damping = damping
        self.mapping = collections.defaultdict(int)
        self.url_dict = collections.defaultdict(list)
        self.score_dict = collections.defaultdict(float)

    @staticmethod
    def download_page(url):
        try:
            req = requests.get(url).content
            return req.decode('utf-8')
        except Exception as e:
            print('Download_page method error: ' + str(e))
            return None

    @staticmethod
    def extract_link(html):
        url_list = []

        try:
            soup = BeautifulSoup(html, 'html.parser')
            href_list = soup.findAll('a')
            for href in href_list:
                real_href = str(href.get('href'))
                if re.match('^/wiki/', real_href) is not None and ':' not in real_href:
                    url_list.append(real_href)

            return url_list
        except (TypeError, UnicodeEncodeError) as e:
            print('Extract_link method error: ' + str(e))
            return None

    def crawl(self):
        input_file = open('./external/count.txt', 'r')
        lines = input_file.readlines()
        input_file.close()

        threads = []
        doc_length = len(lines)
        num_of_thread = int(math.floor(doc_length / THREAD_LIMIT))

        for i in range(num_of_thread):
            threads.append(threading.Thread(target=self.__function, args=(lines[100 * i:100 * (i + 1)],)))
            threads[i].start()

        if doc_length > THREAD_LIMIT * num_of_thread:
            extra_thread = threading.Thread(target=self.__function, args=(lines[THREAD_LIMIT * num_of_thread:doc_length],))
            threads.append(extra_thread)
            extra_thread.start()
            num_of_thread += 1

        for thread in threads:
            thread.join()

        return num_of_thread

    def rank(self):
        score_dict = collections.defaultdict(lambda: 1.0)
        length = len(self.url_dict)
        for node, edges in self.url_dict.items():
            score = 0
            for edge in edges:
                if edge in score_dict:
                    score += self.damping * score_dict[edge] / len(edge)
                else:
                    score += self.damping * 1.0 / length / len(edge)

            score_dict[node] = score + (1 - self.damping) / length

        for url, score in score_dict.items():
            self.score_dict[self.mapping[url]] = score

        with open('./external/wiki_score.txt', 'w', encoding='utf-8') as output:
            output.write(json.dumps(self.score_dict))

    def __function(self, lines):
        for line in lines:
            clean_line = line.replace('\n', '').replace('\r', '')
            tokens = clean_line.split('::')
            url = tokens[1]
            html = Crawler.download_page(url)
            if html is None:
                continue

            url_links = Crawler.extract_link(html)
            if url_links is None:
                continue

            for unique_url in url_links:
                self.mapping[url] = tokens[0]
                self.url_dict[url].append(unique_url)


def main():
    crawler = Crawler()

    t0 = time.time()

    num_of_thread = crawler.crawl()

    print('Done constructing graph....\nComputing relevance using PageRank....')

    crawler.rank()

    t1 = time.time()
    print('Num of valid Doc: ' + str(len(crawler.url_dict)))
    print('Num of Thread: ' + str(num_of_thread))
    print('Processing time: ' + str(t1 - t0))


if __name__ == '__main__':
    main()
