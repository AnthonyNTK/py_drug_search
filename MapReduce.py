import json

"""
Custom MapReduce job which can be deployed to Hadoop cluster in case of much
larger scale of data size and much heavier work load
"""


class MapReduce:
    def __init__(self):
        self.intermediate = {}
        self.result = []

    def emit_intermediate(self, key, value):
        self.intermediate.setdefault(key, [])
        self.intermediate[key].append(value)

    def emit(self, value):
        self.result.append(value) 

    def execute(self, data, mapper, reducer, output):
        for line in data:
            record = json.loads(line)
            mapper(record)

        for key in self.intermediate:
            reducer(key, self.intermediate[key])

        j_encoder = json.JSONEncoder()
        for item in self.result:
            output.write(j_encoder.encode(item) + '\n')
